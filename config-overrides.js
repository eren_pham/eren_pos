const path = require('path')

const {
    override,
    addWebpackAlias,
    disableEsLint,
    addLessLoader,
    addWebpackModuleRule,
} = require('customize-cra')

module.exports = override(
    addWebpackModuleRule({
        test: /\.ya?ml$/,
        type: 'json',
        use: 'yaml-loader',
    }),
    addWebpackAlias({
        '~': path.resolve(__dirname, 'src/'),
    }),
    disableEsLint(),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: {
            '@primary-color': '#1498d5',
        },
    }),
)
