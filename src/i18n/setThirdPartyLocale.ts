import asyncSome from '~/lib/utils/asyncSome'

import { hasCountryCode, stripCountryCode } from './i18n'
import thirdParties from './thirdParties'
import { IThirdParty } from './thirdParties/types'

type LocaleCandidate = (lng: string, third: IThirdParty) => string

const countriedLocaleCandidates: ReadonlyArray<LocaleCandidate> = [
    (lng) => lng,
    (lng, third) =>
        third.LANGUAGE_ONLY_FALLBACK_LOCALE &&
        third.LANGUAGE_ONLY_FALLBACK_LOCALE[stripCountryCode(lng)],
    (lng) => stripCountryCode(lng),
    (_, third) => third.FINAL_FALLBACK_LOCALE,
]

const languageOnlyLocaleCandidates: ReadonlyArray<LocaleCandidate> = [
    (lng, third) =>
        third.LANGUAGE_ONLY_FALLBACK_LOCALE &&
        third.LANGUAGE_ONLY_FALLBACK_LOCALE[lng],
    (lng) => lng,
    (_, third) => third.FINAL_FALLBACK_LOCALE,
]

const setThirdPartyLocale = (
    currentLng: string,
    i18nextInstance: any,
): Promise<ReadonlyArray<any>> => {
    const done: ReadonlyArray<Promise<any>> = thirdParties.map(
        async (thirdParty) => {
            const candidates = hasCountryCode(currentLng)
                ? countriedLocaleCandidates
                : languageOnlyLocaleCandidates

            // console.log('candidates', candidates)

            let translation

            let lng
            await asyncSome(candidates, async (candidate: LocaleCandidate) => {
                lng = candidate(currentLng, thirdParty)

                // console.log('lng', lng, currentLng, thirdParty)

                if (!lng) {
                    return Promise.reject()
                }

                return (translation = await thirdParty.load({
                    i18nextInstance,
                    lng: lng as string,
                }))
            })

            // console.log('thirdParty', index, lng, translation, candidates)

            // if (translation !== 'yup') {
            return thirdParty.set({ lng, translation })
            // }
        },
    )

    return Promise.all(done)
}

export default setThirdPartyLocale
