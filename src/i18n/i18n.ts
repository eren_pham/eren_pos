import { DEFAULT_LANG } from '~/modules/Locale'
import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

import setThirdPartyLocale from './setThirdPartyLocale'
import DynamicImportBackend from '~/lib/i18next/DynamicImportBackend'

export const I18N_DEFAULT_NAMESPACE = 'common'
export const I18N_VALIDATION_NAMESPACE = 'validation'

export const I18N_NAMESPACES: string[] = [
    I18N_DEFAULT_NAMESPACE,
    I18N_VALIDATION_NAMESPACE,
]

export const ALLOWED_BACKEND_LOCALES: string[] = ['en', 'vi']

export function hasCountryCode(lng: string): boolean {
    return lng.includes('-')
}

export function stripCountryCode(lng: string): string {
    return lng.split('-')[0]
}

// const resources: any = {}
// for (let i = 0; i < ALLOWED_BACKEND_LOCALES.length; i++) {
//     const lng = ALLOWED_BACKEND_LOCALES[i]
//     resources[lng] = {}
//     for (let j = 0; j < I18N_NAMESPACES.length; j++) {
//         const ns = I18N_NAMESPACES[j]
//         const langData = import('~/i18n/locale/' + lng + '/' + ns + '.yml')
//         resources[lng][ns] = await langData
//     }
// }

i18n.use(LanguageDetector)
    .use(DynamicImportBackend)
    .use(initReactI18next)
    .init({
        lng: DEFAULT_LANG,
        backend: {
            loaders: {
                // prettier-ignore
                en: {
                    // @ts-ignore
                    common: () => import('./locale/en/common.yml'),
                    // @ts-ignore
                    validation: () => import('./locale/en/validation.yml')
                },
                // prettier-ignore
                vi: {
                    // @ts-ignore
                    common: () => import('./locale/vi/common.yml'),
                    // @ts-ignore
                    validation: () => import('./locale/vi/validation.yml')
                }
            },
        },
        debug: process.env.NODE_ENV === 'development',
        defaultNS: I18N_DEFAULT_NAMESPACE,
        fallbackLng: DEFAULT_LANG,
        interpolation: {
            escapeValue: false,
        },
        load: 'languageOnly',
        ns: I18N_NAMESPACES,
        react: {
            wait: false,
        },
        whitelist: ALLOWED_BACKEND_LOCALES,
    })

i18n.on('languageChanged', async (lng) => {
    await setThirdPartyLocale(lng, i18n)
})

export default i18n
