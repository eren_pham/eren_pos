import moment from 'moment'

import { ILoadOptions, ISetOptions, IThirdParty } from './types'

class Moment implements IThirdParty {
    public readonly FINAL_FALLBACK_LOCALE = 'vi'
    public readonly LANGUAGE_ONLY_FALLBACK_LOCALE = {}

    public load({ lng }: ILoadOptions): Promise<any> {
        // `en.js` and `en-us.js` does not exist, but `moment.locale('en')` sets things equivalent to 'en-us'
        if (lng === 'en') return Promise.resolve()
        // $FlowFixMe
        return import(
            `moment/locale/${lng.toLowerCase()}` /* webpackChunkName: "moment-locale-[request]" */
        )
    }

    public set({ lng }: ISetOptions): void {
        moment.locale(lng)
    }
}

export default new Moment()
