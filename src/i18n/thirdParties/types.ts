export interface ILoadOptions {
    readonly lng: string
    readonly i18nextInstance: any
}

export interface ISetOptions {
    readonly lng?: string
    readonly translation: any
}

export interface IThirdParty {
    readonly LANGUAGE_ONLY_FALLBACK_LOCALE: {
        readonly [locale: string]: string
    }
    readonly FINAL_FALLBACK_LOCALE: string
    readonly load: (options: ILoadOptions) => Promise<any>
    readonly set: (options: ISetOptions) => void
}
