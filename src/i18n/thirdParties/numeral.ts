import numeral from 'numeral'

import { ILoadOptions, ISetOptions, IThirdParty } from './types'

class Numeral implements IThirdParty {
    public readonly LANGUAGE_ONLY_FALLBACK_LOCALE = {
        // `nl.js` doesn't exist so fallback to `nl-be.js`.
        // Leaving this just for an example code though currently this line won't hit.
        nl: 'nl-be',
    }
    public readonly FINAL_FALLBACK_LOCALE = 'vi'

    public load({ lng }: ILoadOptions): Promise<any> {
        // `en.js` and `en-us.js` does not exist, but `numeral.locale('en')` sets things equivalent to 'en-us'
        if (lng === 'en') return Promise.resolve()
        // $FlowFixMe
        return import(
            `numeral/locales/${lng.toLowerCase()}` /* webpackChunkName: "numeral-locale-[request]" */
        )
    }

    public set({ lng }: ISetOptions): void {
        numeral.locale(lng)
    }
}

export default new Numeral()
