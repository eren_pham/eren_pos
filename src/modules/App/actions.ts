import { Themes } from '~/styles/themes'

import * as actionTypes from './actionTypes'
import { AppConfig, Redirect } from './types'

export const reload = () => ({
    type: actionTypes.APP_RELOAD,
    payload: {},
})

export const redirect = (redirect: Redirect) => ({
    type: actionTypes.APP_REDIRECT,
    payload: { redirect },
})

export const changeDirty = (isDirty: boolean) => ({
    type: actionTypes.APP_CHANGE_DIRTY,
    payload: { isDirty },
})

export const changeTheme = (theme: Themes) => ({
    type: actionTypes.APP_CHANGE_THEME,
    payload: { theme },
})

export const persistConfig = () => ({
    type: actionTypes.APP_PERSIST_CONFIG,
    payload: {},
})

export const restoreConfig = (config: AppConfig) => ({
    type: actionTypes.APP_RESTORE_CONFIG,
    payload: { config },
})

export interface Actions {
    readonly [actionTypes.APP_RELOAD]: ReturnType<typeof reload>
    readonly [actionTypes.APP_REDIRECT]: ReturnType<typeof redirect>
    readonly [actionTypes.APP_CHANGE_DIRTY]: ReturnType<typeof changeDirty>
    readonly [actionTypes.APP_CHANGE_THEME]: ReturnType<typeof changeTheme>
    readonly [actionTypes.APP_PERSIST_CONFIG]: ReturnType<typeof persistConfig>
    readonly [actionTypes.APP_RESTORE_CONFIG]: ReturnType<typeof restoreConfig>
}
export type Action = Actions[keyof Actions]
