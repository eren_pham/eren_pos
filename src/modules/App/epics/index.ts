import { combineEpics } from 'redux-observable'

/**
 * export combine epics
 */
export const appEpic = combineEpics()

export default appEpic
