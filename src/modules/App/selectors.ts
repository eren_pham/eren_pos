import { Themes, themes } from '~/styles/themes'
import { RootState } from '~/types/redux'
import { createSelector } from 'reselect'

/**
 * Selector: App module
 * @returns {AppState}
 */
export const appState = (state: RootState) => state.app

/**
 * Selector: AppConfig
 * @return {AppConfig}
 */
export const appConfig = createSelector(appState, (app) => app.config)

/**
 * Selector: Current theme
 * @return {Themes}
 */
export const currentTheme = createSelector(appConfig, (config) => config.theme)

/**
 * Selector: Current theme object
 */
export const currentThemeObject = createSelector(currentTheme, (theme) => ({
    mode: theme,
    ...(themes[theme] ? themes[theme]() : themes.light()),
}))

/**
 * Selector: Available themes
 */
export const availableThemes = (): ReadonlyArray<Themes> =>
    Object.keys(themes) as ReadonlyArray<Themes>
