import { Reducer } from 'redux'

import * as actionTypes from './actionTypes'
import { Action, Actions } from './actions'
import { State } from './types'

/**
 * App initial state
 */
export const initialState: State = {
    config: {
        isDirty: false,
        theme: 'light',
    },
}

/**
 * Reducer: App reducer
 */
export const reducer: Reducer<State, Action> = (
    state = initialState,
    action,
) => {
    const actions = action.payload as Actions
    const { type } = action

    switch (type) {
        case actionTypes.APP_CHANGE_DIRTY:
            return {
                ...state,
                config: {
                    ...state.config,
                    isDirty: actions[type].payload.isDirty,
                },
            }

        case actionTypes.APP_CHANGE_THEME:
            return {
                ...state,
                config: {
                    ...state.config,
                    theme: actions[type].payload.theme,
                    isDirty: true,
                },
            }

        case actionTypes.APP_PERSIST_CONFIG:
            return {
                ...state,
                config: { ...state.config, isDirty: false },
            }

        case actionTypes.APP_RESTORE_CONFIG:
            return {
                ...state,
                config: { ...state.config, ...actions[type].payload.config },
            }

        default:
            return state
    }
}
