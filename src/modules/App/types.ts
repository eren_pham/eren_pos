import { Themes } from '~/styles/themes'

export interface AppConfig {
    readonly theme: Themes
    readonly isDirty: boolean
}

export interface State {
    readonly config: AppConfig
}

export interface Redirect {
    readonly path: string
    readonly query?: object
}
