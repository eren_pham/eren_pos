import { Reducer } from 'redux'

import * as actionTypes from './actionTypes'
import { Action, Actions } from './Actions'
import { Language, languages, State } from './types'

// This should be set to en, but we'll set it to vi for now,
// to prevent loading the additional translation resources for our main target country
export const DEFAULT_LANG: Language = 'en'

export const initialState: State = {
    isReady: false,
    lang: DEFAULT_LANG,
    isDirty: false,
    languages,
}

export const reducer: Reducer<State, Action> = (
    state = initialState,
    action,
) => {
    const actions = action.payload as Actions
    const { type } = action

    switch (type) {
        case actionTypes.LOCALE_CHANGE_LANGUAGE:
            return { ...state, lang: actions[type].payload.lang, isDirty: true }

        case actionTypes.LOCALE_READY:
            return { ...state, isReady: true }

        case actionTypes.LOCALE_PERSIST:
            return {
                ...state,
                isDirty: false,
            }

        case actionTypes.LOCALE_RESTORE:
            return {
                ...state,
                ...actions[type].payload.locale,
            }

        default:
            return state
    }
}
