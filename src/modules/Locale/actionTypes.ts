export const LOCALE_READY = '@@locale/READY'
export const LOCALE_CHANGE_LANGUAGE = '@@locale/CHANGE_LANGUAGE'
export const LOCALE_PERSIST = '@@locale/PERSIST_LOCALE'
export const LOCALE_RESTORE = '@@locale/RESTORE_LOCALE'
