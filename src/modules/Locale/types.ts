const en = 'English'
const vi = 'Tiếng Việt'

export const languages = { en, vi }

type Languages = typeof languages

export type Language = keyof Languages

export interface State {
    readonly isReady: boolean
    readonly lang: Language
    readonly isDirty: boolean
    readonly languages: typeof languages
}
