import * as actionTypes from './actionTypes'
import { State, Language } from './types'

export const ready = () => ({
    type: actionTypes.LOCALE_READY,
    payload: {},
})

export const changeLanguage = (lang: Language) => ({
    type: actionTypes.LOCALE_CHANGE_LANGUAGE,
    payload: { lang },
})

export const persistLocale = () => ({
    type: actionTypes.LOCALE_PERSIST,
    payload: {},
})

export const restoreLocale = (locale: State) => ({
    type: actionTypes.LOCALE_RESTORE,
    payload: { locale },
})

export interface Actions {
    readonly [actionTypes.LOCALE_READY]: ReturnType<typeof ready>
    readonly [actionTypes.LOCALE_CHANGE_LANGUAGE]: ReturnType<
        typeof changeLanguage
    >
    readonly [actionTypes.LOCALE_PERSIST]: ReturnType<typeof persistLocale>
    readonly [actionTypes.LOCALE_RESTORE]: ReturnType<typeof restoreLocale>
}

export type Action = Actions[keyof Actions]
