import { RootState } from '~/types/redux'
import { createSelector } from 'reselect'

/**
 * Selector: Locale module
 * @returns {LocaleState}
 */
export const localeState = (state: RootState) => state.locale

/**
 * Selector: Current language
 */
export const currentLanguage = createSelector(
    localeState,
    (locale) => locale.lang,
)
