import * as actionTypes from './actionTypes'
import {
    AddToCartParams,
    AssignCustomerParams,
    CartItemInterface,
    CartLocalInterface,
    CustomerInterface,
    InformationInterface,
    PosInterface,
} from './types'
import { POS_NEW_CART } from './actionTypes'

/**
 * get website information
 * @returns {type: string, payload: {}}
 */
export const getWebsiteInformation = () => ({
    type: actionTypes.WEBSITE_GET_INFORMATION,
    payload: {},
})

/**
 * set website information
 * @param information
 * @returns {type: string, payload: {information: WebsiteInformationResultInterface}}
 */
export const setWebsiteInformation = (information: InformationInterface) => ({
    type: actionTypes.WEBSITE_SET_INFORMATION,
    payload: information,
})

/**
 * assign pos
 * @param pos
 * @returns {type: string, payload: {pos: PosInterface}}
 */
export const assignPos = (pos: PosInterface | null) => ({
    type: actionTypes.POS_ASSIGN_POS,
    payload: pos,
})

/**
 * set website information
 * @param quote
 * @returns {type: string, payload: {pos: PosInterface}}
 */
export const setQuote = (quote: string | null) => ({
    type: actionTypes.POS_CREATE_QUOTE,
    payload: quote,
})

/**
 * get cart
 * @returns {type: string, payload: {}}
 */
export const newCart = () => ({
    type: actionTypes.POS_NEW_CART,
    payload: {},
})

/**
 * get cart
 * @returns {type: string, payload: {}}
 */
export const getCart = () => ({
    type: actionTypes.POS_GET_CART,
    payload: {},
})

/**
 * add to cart
 * @param item
 * @returns {type: string, payload: {item: AddToCartParams}}
 */
export const addToCart = (item: AddToCartParams) => ({
    type: actionTypes.POS_ADD_TO_CART,
    payload: item,
})

/**
 * update cart
 * @param cart
 * @returns {type: string, payload: {pos: PosInterface}}
 */
export const updateCart = (cart: CartLocalInterface) => ({
    type: actionTypes.POS_UPDATE_CART,
    payload: cart,
})

/**
 * delete cart
 * @param itemId
 * @returns {type: string, payload: {item_id: number}}
 */
export const deleteCart = (itemId: number) => ({
    type: actionTypes.POS_DELETE_CART,
    payload: itemId,
})

/**
 * assign customer
 * @param assign
 * @returns {type: string, payload: {assign: AssignCustomerParams}}
 */
export const startAssignCustomer = (assign: AssignCustomerParams) => ({
    type: actionTypes.POS_START_ASSIGN_CUSTOMER,
    payload: assign,
})

/**
 * assign customer
 * @returns {type: string, payload: {}}
 */
export const endAssignCustomer = () => ({
    type: actionTypes.POS_END_ASSIGN_CUSTOMER,
    payload: {},
})

/**
 * set customer
 * @param customer
 * @returns {type: string, payload: {customer: CustomerInterface}}
 */
export const setCustomer = (customer: CustomerInterface) => ({
    type: actionTypes.POS_SET_CUSTOMER,
    payload: customer,
})

export interface Actions {
    readonly [actionTypes.WEBSITE_GET_INFORMATION]: ReturnType<
        typeof getWebsiteInformation
    >
    readonly [actionTypes.WEBSITE_SET_INFORMATION]: ReturnType<
        typeof setWebsiteInformation
    >
    readonly [actionTypes.POS_ASSIGN_POS]: ReturnType<typeof assignPos>
    readonly [actionTypes.POS_CREATE_QUOTE]: ReturnType<typeof setQuote>
    readonly [actionTypes.POS_NEW_CART]: ReturnType<typeof newCart>
    readonly [actionTypes.POS_GET_CART]: ReturnType<typeof getCart>
    readonly [actionTypes.POS_ADD_TO_CART]: ReturnType<typeof addToCart>
    readonly [actionTypes.POS_UPDATE_CART]: ReturnType<typeof updateCart>
    readonly [actionTypes.POS_DELETE_CART]: ReturnType<typeof deleteCart>
    readonly [actionTypes.POS_START_ASSIGN_CUSTOMER]: ReturnType<
        typeof startAssignCustomer
    >
    readonly [actionTypes.POS_END_ASSIGN_CUSTOMER]: ReturnType<
        typeof endAssignCustomer
    >
    readonly [actionTypes.POS_SET_CUSTOMER]: ReturnType<typeof setCustomer>
}

export type Action = Actions[keyof Actions]
