import { RootState } from '~/types/redux'
import { createSelector } from 'reselect'

export const root = (state: RootState) => state.webpos

export const information = createSelector(root, (app) => app.information)

export const pos = createSelector(root, (app) => app.pos)

export const quoteId = createSelector(root, (app) => app.quote_id)

export const cart = createSelector(root, (app) => app.cart)

export const loading = createSelector(root, (app) => app.loading)

export const customer = createSelector(root, (app) => app.customer)
