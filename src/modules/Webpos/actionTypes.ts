export const WEBSITE_GET_INFORMATION = '@@website/GET_INFORMATION'
export const WEBSITE_SET_INFORMATION = '@@website/SET_INFORMATION'

export const POS_GET_LIST_POS = '@@pos/GET_LIST_POS'
export const POS_ASSIGN_POS = '@@pos/ASSIGN_POS'

export const POS_CREATE_QUOTE = '@@pos/CREATE_QUOTE'
export const POS_NEW_CART = '@@pos/NEW_CART'
export const POS_GET_CART = '@@pos/GET_CART'
export const POS_ADD_TO_CART = '@@pos/ADD_TO_CART'
export const POS_UPDATE_CART = '@@pos/UPDATE_CART'
export const POS_DELETE_CART = '@@pos/DELETE_CART'

export const POS_START_ASSIGN_CUSTOMER = '@@pos/START_ASSIGN_CUSTOMER'
export const POS_END_ASSIGN_CUSTOMER = '@@pos/END_ASSIGN_CUSTOMER'

export const POS_SET_CUSTOMER = '@@pos/SET_CUSTOMER'
