import { Observable } from 'rxjs'
import { ActionsObservable, ofType } from 'redux-observable'
import { map, mergeMap } from 'rxjs/operators'
import {
    Action,
    endAssignCustomer,
    setCustomer,
    setQuote,
    updateCart,
} from '../actions'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import {
    POS_ADD_TO_CART,
    POS_DELETE_CART,
    POS_GET_CART,
    POS_NEW_CART,
    POS_START_ASSIGN_CUSTOMER,
} from '../actionTypes'
import * as Api from '~/data/Api'
import {
    AddToCartParams,
    AssignCustomerParams,
    CartLocalInterface,
    CustomerInterface,
} from '~/modules/Webpos'
import { CART_ID, QUOTE } from '~/modules/Webpos/constants'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * get cart epic
 * @param action$
 * @returns {Observable<any>}
 */
export const newCartEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(POS_NEW_CART),
        mergeMap(() => Api.createGuestCart()),
        map((value) => setQuote(value.response)),
    )

/**
 * get cart epic
 * @param action$
 * @returns {Observable<any>}
 */
export const getCartEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(POS_GET_CART),
        mergeMap(() => [storage.get(QUOTE)]),
        mergeMap((value) => Api.getCartInfo(value)),
        map((value) => {
            const cartRemote: CartLocalInterface = {}
            const customer: CustomerInterface = {
                id: value.customer.id,
                email: value.customer.email,
                firstname: value.customer.firstname,
                lastname: value.customer.lastname,
            }
            storage.set(CART_ID, value.id)
            value.items.forEach((item) => {
                cartRemote[item.item_id] = item
            })
            setCustomer(customer)
            return updateCart(cartRemote)
        }),
    )

/**
 * add to cart epic
 * @param action$
 * @returns {Observable<any>}
 */
export const addToCartEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(POS_ADD_TO_CART),
        mergeMap((value) => {
            const payload = value.payload as AddToCartParams
            return [
                {
                    quoteId: storage.get(QUOTE),
                    sku: payload.sku,
                    qty: payload.qty,
                },
            ]
        }),
        mergeMap((value) => Api.addToCart(value)),
        mergeMap(() => [storage.get(QUOTE)]),
        mergeMap((value) => Api.getCartInfo(value)),
        map((value) => {
            const cartRemote: CartLocalInterface = {}
            value.items.forEach((item) => {
                cartRemote[item.item_id] = item
            })
            return updateCart(cartRemote)
        }),
    )

/**
 * delete cart epic
 * @param action$
 * @returns {Observable<any>}
 */
export const deleteEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(POS_DELETE_CART),
        mergeMap((value) => {
            return [
                {
                    itemId: value.payload,
                    quoteId: storage.get(QUOTE),
                },
            ]
        }),
        mergeMap((value) => {
            const id = value.itemId + ''
            return Api.deleteItem(value.quoteId, parseInt(id))
        }),
        mergeMap(() => [storage.get(QUOTE)]),
        mergeMap((value) => Api.getCartInfo(value)),
        map((value) => {
            const cartRemote: CartLocalInterface = {}
            value.items.forEach((item) => {
                cartRemote[item.item_id] = item
            })
            return updateCart(cartRemote)
        }),
    )

/**
 * assign customer
 * @param action$
 * @returns {Observable<any>}
 */
export const assignCustomerEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(POS_START_ASSIGN_CUSTOMER),
        mergeMap((assign) => {
            const payload = assign.payload as AssignCustomerParams
            return Api.assignCustomer(payload)
        }),
        map(() => {
            const cartId = storage.get(CART_ID)
            const quoteId = storage.get(QUOTE)
            storage.set(QUOTE, cartId)
            storage.set(CART_ID, quoteId)
            return endAssignCustomer()
        }),
    )
