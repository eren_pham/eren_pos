import { Observable, of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { ActionsObservable, ofType } from 'redux-observable'
import { map, mergeMap } from 'rxjs/operators'
import { Action, setWebsiteInformation } from '../actions'
import { InformationInterface } from '../types'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { WEBSITE_GET_INFORMATION } from '../actionTypes'
import { WEBSITE_INFORMATION } from '~/modules/Webpos/constants'
import * as Api from '~/data/Api'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * get information epic
 * @param action$
 * @returns {Observable<any>}
 */
export const getInformationEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(WEBSITE_GET_INFORMATION),
        mergeMap((): any => [storage.get(WEBSITE_INFORMATION)]),
        mergeMap((value) => {
            if (value) return of(value as InformationInterface)
            return Api.getWebsiteInformation()
        }),
        map((value) => setWebsiteInformation(value)),
    )
