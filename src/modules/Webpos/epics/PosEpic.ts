import { Observable } from 'rxjs'
import { ActionsObservable, ofType } from 'redux-observable'
import { map, mergeMap } from 'rxjs/operators'
import { Action, assignPos, setWebsiteInformation } from '../actions'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { POS_ASSIGN_POS, POS_GET_LIST_POS } from '../actionTypes'
import * as Api from '~/data/Api'
import { PosInterface } from '~/modules/Webpos'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * assign pos epic
 * @param action$
 * @returns {Observable<any>}
 */
export const assignPosEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(ofType(POS_ASSIGN_POS))
