import { combineEpics } from 'redux-observable'
import { getInformationEpic } from './WebsiteEpic'
import {
    addToCartEpic,
    deleteEpic,
    newCartEpic,
    getCartEpic,
    assignCustomerEpic,
} from './CartEpic'

/**
 * export combine epics
 */
export const webposEpic = combineEpics(
    getInformationEpic,
    newCartEpic,
    getCartEpic,
    addToCartEpic,
    deleteEpic,
    assignCustomerEpic,
)

export default webposEpic
