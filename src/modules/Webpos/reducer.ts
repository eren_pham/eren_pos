import { Reducer } from 'redux'
import { Action } from './actions'
import * as actionTypes from './actionTypes'
import {
    State,
    InformationInterface,
    PosInterface,
    CartLocalInterface,
    CartItemInterface,
    CustomerInterface,
} from './types'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import {
    CUSTOMER,
    POS,
    QUOTE,
    WEBSITE_INFORMATION,
} from '~/modules/Webpos/constants'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * Webpos initial state
 */
export const initialState: State = {
    information: {
        logo_url: null,
        primary_color: null,
        second_color: null,
    },
    pos: null,
    quote_id: null,
    cart: null,
    customer: {
        id: null,
        email: null,
        firstname: null,
        lastname: null,
    },
    loading: false,
}

/**
 * Reducer: WebposID reducer
 */
export const reducer: Reducer<State, Action> = (
    state = initialState,
    action,
) => {
    const { type, payload } = action

    switch (type) {
        case actionTypes.WEBSITE_SET_INFORMATION:
            const information = payload as InformationInterface
            if (information.logo_url === null) {
                information.logo_url = '/PwaPOS-favicon.png'
            }
            storage.set(WEBSITE_INFORMATION, information)
            return {
                ...state,
                information,
            }

        case actionTypes.POS_ASSIGN_POS:
            const pos = payload as PosInterface
            storage.set(POS, pos)
            return {
                ...state,
                pos: pos,
            }

        case actionTypes.POS_CREATE_QUOTE:
            const quoteId = payload as string
            storage.set(QUOTE, quoteId)
            return {
                ...state,
                quote_id: quoteId,
            }

        case actionTypes.POS_GET_CART:
        case actionTypes.POS_ADD_TO_CART:
        case actionTypes.POS_DELETE_CART:
        case actionTypes.POS_START_ASSIGN_CUSTOMER:
            return {
                ...state,
                loading: true,
            }

        case actionTypes.POS_END_ASSIGN_CUSTOMER:
            return {
                ...state,
                loading: false,
            }

        case actionTypes.POS_SET_CUSTOMER:
            console.log('wtf', payload)
            storage.set(CUSTOMER, payload)
            return {
                ...state,
                customer: payload as CustomerInterface,
                loading: false,
            }

        case actionTypes.POS_UPDATE_CART:
            return {
                ...state,
                cart: payload as CartLocalInterface,
                loading: false,
            }

        default:
            return state
    }
}
