export interface State {
    readonly information: InformationInterface
    readonly pos: PosInterface | null
    readonly quote_id: string | null
    readonly cart: CartLocalInterface | null
    readonly customer: CustomerInterface
    readonly loading: boolean
}

export interface InformationInterface {
    logo_url: string | null
    primary_color: string | null
    second_color: string | null
}

export interface PosInterface {
    pos_id: number
    pos_name: string
}

export interface ProductResultInterface {
    items: ProductInterface[]
    search_criteria: any
    total_count: number
}

export type ProductType = 'simple' | 'configurable'

export interface ProductInterface {
    id: string
    name: string
    image: string
    sku: string
    price: number
    store_id: number
    visibility: string
    status: string
    category: any
    weight: any
    type_id: ProductType
    childItems: ProductChildInterface[]
}

export interface ProductChildInterface {
    entity_id: string
    type_id: ProductType
    sku: string
    name: string
    color: {
        option_id: string
        label: string
        option_value: string
    }
    size: {
        option_id: string
        label: string
        option_value: string
    }
}

export interface AddToCartParams {
    sku: string
    qty: number
}

export interface CartItemInterface {
    item_id: number
    sku: string
    qty: number
    name: string
    price: number
    product_type: ProductType
    quote_id: string
    extension_attributes: {
        image_url: string
    }
}

export interface CartLocalInterface {
    [id: string]: CartItemInterface
}

export interface CustomerInterface {
    id: number | null
    email: string | null
    firstname: string | null
    lastname: string | null
}

export interface BillingAddressInterface {
    id: number
    region: string | null
    region_id: any
    region_code: any
    country_id: any
    street: any
    telephone: any
    postcode: any
    city: any
    firstname: any
    lastname: any
    email: any
    same_as_billing: any
    save_in_address_book: any
}

export interface CurrencyInterface {
    global_currency_code: string
    base_currency_code: string
    store_currency_code: string
    quote_currency_code: string
    store_to_base_rate: number
    store_to_quote_rate: number
    base_to_global_rate: number
    base_to_quote_rate: number
}

export interface CartInfo {
    id: number
    created_at: string
    updated_at: string
    is_active: boolean
    is_virtual: boolean
    items: CartItemInterface[]
    items_count: number
    items_qty: number
    customer: CustomerInfoInterface
    billing_address: BillingAddressInterface
    orig_order_id: number
    currency: CurrencyInterface
    customer_is_guest: boolean
    customer_note_notify: boolean
    customer_tax_class_id: number
    store_id: number
    extension_attributes: any
}

export interface CustomerInfoInterface {
    id: number
    group_id: number
    default_billing: string
    default_shipping: string
    created_at: string
    updated_at: string
    created_in: string
    email: string
    firstname: string
    lastname: string
    gender: number
    store_id: number
    website_id: number
    addresses: any
    disable_auto_group_change: number
    extension_attributes: {
        is_subscribed: boolean
    }
}

export interface CustomerResultInterface {
    items: CustomerInfoInterface[]
    search_criteria: {
        filter_groups: any
        page_size: number
        current_page: number
    }
    total_count: number
}

export interface AssignCustomerParams {
    customerId: number
    quoteId: string
}
