import { Observable, of } from 'rxjs'
import { ActionsObservable, ofType } from 'redux-observable'
import { catchError, map, mergeMap } from 'rxjs/operators'
import { Action, userClickLogout, userLoginSuccess } from '../actions'
import {
    FORCE_SIGN_OUT,
    USER_CLICK_LOGIN,
    USER_RESTORE_LOGIN,
} from '../actionTypes'
import { StaffInterface, UserLoginForm } from '~/modules/WebposID'
import * as Api from '~/data/Api'
import Warning from '~/lib/utils/Warning'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { STAFF } from '~/modules/WebposID/constants'
import Message from '~/lib/utils/Message'
import { CUSTOMER, POS, QUOTE } from '~/modules/Webpos/constants'
import { CustomerInterface, PosInterface } from '~/modules/Webpos'
import { assignPos, setCustomer, setQuote } from '~/modules/Webpos/actions'
import { push, replace } from 'connected-react-router'
import routeNames from '~/routes/routeNames'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * login epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userLoginEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(USER_CLICK_LOGIN),
        mergeMap((data) => {
            return [data.payload as UserLoginForm]
        }),
        mergeMap((value) =>
            Api.userLogin(value).pipe(
                map((value) => {
                    Message('Login successful')
                    return userLoginSuccess(value.response)
                }),
                catchError((err: any, caught) => {
                    Warning(err)
                    return of(userClickLogout())
                }),
            ),
        ),
    )

/**
 * restore login epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userRestoreLoginEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(USER_RESTORE_LOGIN),
        mergeMap((): any => [storage.get(STAFF)]),
        mergeMap((value) => {
            if (value) return of(value as StaffInterface)
            return []
        }),
        map((value) => (value ? userLoginSuccess(value) : userClickLogout())),
    )

/**
 * restore pos epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userRestorePosEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(USER_RESTORE_LOGIN),
        mergeMap((): any => [storage.get(POS)]),
        mergeMap((value) => {
            if (value) return of(value as PosInterface)
            return []
        }),
        map((value) => assignPos(value ? value : null)),
    )

/**
 * restore quote epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userRestoreQuoteEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(USER_RESTORE_LOGIN),
        mergeMap((): any => [storage.get(QUOTE)]),
        mergeMap((value) => {
            if (value) return of(value as string)
            return []
        }),
        map((value) => setQuote(value ? value : null)),
    )

/**
 * restore customer epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userRestoreCustomerEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(USER_RESTORE_LOGIN),
        mergeMap((): any => [storage.get(CUSTOMER)]),
        mergeMap((value) => {
            if (value) return of(value as CustomerInterface)
            return []
        }),
        map((value) => setCustomer(value)),
    )

/**
 * restore pos epic
 * @param action$
 * @returns {Observable<any>}
 */
export const userLogoutEpic = (action$: ActionsObservable<Action>) =>
    action$.pipe(
        ofType(FORCE_SIGN_OUT),
        map(() => assignPos(null)),
    )
