import { combineEpics } from 'redux-observable'
import {
    userLoginEpic,
    userLogoutEpic,
    userRestoreCustomerEpic,
    userRestoreLoginEpic,
    userRestorePosEpic,
    userRestoreQuoteEpic,
} from './UserLoginEpic'

/**
 * export combine epics
 */
export const webposEpic = combineEpics(
    userLoginEpic,
    userRestoreLoginEpic,
    userRestorePosEpic,
    userRestoreQuoteEpic,
    userRestoreCustomerEpic,
    userLogoutEpic,
)

export default webposEpic
