import { RootState } from '~/types/redux'
import { createSelector } from 'reselect'
import { AuthStatuses } from '~/modules/WebposID/types'

export const root = (state: RootState) => state.webposID

export const staff = createSelector(root, (selector) => selector.staff)

export const sessionToken = createSelector(
    staff,
    (selector) => selector?.session_token,
)

export const accessToken = createSelector(
    staff,
    (selector) => selector?.access_token,
)

export const isSignedOut = createSelector(
    root,
    (auth) =>
        auth.status === AuthStatuses.INITIALIZING ||
        auth.status === AuthStatuses.SIGNING_IN ||
        auth.status === AuthStatuses.SIGNED_OUT,
)

export const isSignedIn = createSelector(
    root,
    (auth) =>
        auth.status === AuthStatuses.SIGNING_OUT ||
        auth.status === AuthStatuses.SIGNED_IN,
)

export const isProcessing = createSelector(
    root,
    (auth) =>
        auth.status === AuthStatuses.INITIALIZING ||
        auth.status === AuthStatuses.SIGNING_IN ||
        auth.status === AuthStatuses.SIGNING_OUT,
)
