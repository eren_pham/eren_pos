import * as actionTypes from './actionTypes'
import { StaffInterface, UserLoginForm } from './types'
import { FORCE_SIGN_OUT } from './actionTypes'

/**
 * action click login
 * @param form
 * @returns {type: string, payload: {username: string, password: string}}
 */
export const userClickLogin = (form: UserLoginForm) => ({
    type: actionTypes.USER_CLICK_LOGIN,
    payload: form,
})

/**
 * action login success
 * @param staff
 * @returns {type: string, payload: {staff: LoginResultInterface}}
 */
export const userLoginSuccess = (staff: StaffInterface) => ({
    type: actionTypes.USER_LOGIN_RESULT,
    payload: staff,
})

/**
 * action restore login
 * @returns {type: string, payload: {}}
 */
export const userRestoreLogin = () => ({
    type: actionTypes.USER_RESTORE_LOGIN,
    payload: {},
})

/**
 * action continue login
 * @returns {type: string, payload: {}}
 */
export const userClickLogout = () => ({
    type: actionTypes.FORCE_SIGN_OUT,
    payload: {},
})

export interface Actions {
    readonly [actionTypes.USER_CLICK_LOGIN]: ReturnType<typeof userClickLogin>
    readonly [actionTypes.USER_LOGIN_RESULT]: ReturnType<
        typeof userLoginSuccess
    >
    readonly [actionTypes.USER_RESTORE_LOGIN]: ReturnType<
        typeof userRestoreLogin
    >
    readonly [actionTypes.FORCE_SIGN_OUT]: ReturnType<typeof userClickLogout>
}
export type Action = Actions[keyof Actions]
