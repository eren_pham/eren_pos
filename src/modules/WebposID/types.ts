export enum AuthStatuses {
    INITIALIZING = 'INITIALIZING',
    SIGNING_IN = 'SIGNING_IN',
    SIGNED_IN = 'SIGNED_IN',
    SIGNING_OUT = 'SIGNING_OUT',
    SIGNED_OUT = 'SIGNED_OUT',
}

export interface State {
    readonly isFetching: boolean
    readonly status: AuthStatuses
    readonly staff: StaffInterface | null
}

export interface StaffInterface {
    readonly staff_id: number
    readonly staff_name: string
    readonly access_token: string
    readonly session_token: string
}

export interface UserLoginForm {
    readonly username: string
    readonly password: string
}
