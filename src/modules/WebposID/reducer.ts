import { Reducer } from 'redux'
import { Action } from './actions'
import {
    FORCE_SIGN_OUT,
    USER_CLICK_LOGIN,
    USER_LOGIN_RESULT,
} from './actionTypes'
import { AuthStatuses, StaffInterface, State } from './types'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { STAFF } from './constants'
import { POS } from '~/modules/Webpos/constants'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

/**
 * WebposID initial state
 */
export const initialState: State = {
    isFetching: false,
    status: AuthStatuses.SIGNED_OUT,
    staff: null,
}

/**
 * Reducer: WebposID reducer
 */
export const reducer: Reducer<State, Action> = (
    state = initialState,
    action,
) => {
    const { type, payload } = action

    switch (type) {
        case USER_CLICK_LOGIN:
            return {
                ...state,
                status: AuthStatuses.SIGNING_IN,
                isFetching: true,
            }

        case USER_LOGIN_RESULT:
            const staff = payload as StaffInterface
            storage.set(STAFF, staff)
            return {
                ...state,
                staff,
                status: AuthStatuses.SIGNED_IN,
                isFetching: false,
            }

        case FORCE_SIGN_OUT:
            storage.clear()
            return initialState

        default:
            return state
    }
}
