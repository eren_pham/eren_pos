export const USER_CLICK_LOGIN = '@@user/CLICK_LOGIN'
export const USER_LOGIN_RESULT = '@@user/LOGIN_RESULT'
export const USER_RESTORE_LOGIN = '@@user/RESTORE_LOGIN'
export const FORCE_SIGN_OUT = '@@logout/FORCE_SIGN_OUT'
