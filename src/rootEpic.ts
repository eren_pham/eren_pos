import { combineEpics } from 'redux-observable'
import { webposEpic } from '~/modules/Webpos'
import { appEpic } from '~/modules/App'
import { webposIDEpic } from '~/modules/WebposID'

const rootEpic = combineEpics(appEpic, webposEpic, webposIDEpic)

export default rootEpic
