import { Action as AppAction, State as AppState } from '~/modules/App'
import { Action as LocaleAction, State as LocaleState } from '~/modules/Locale'
import { Action as WebposAction, State as WebposState } from '~/modules/Webpos'
import {
    Action as WebposIDAction,
    State as WebposIDState,
} from '~/modules/WebposID'
import { RouterState } from 'connected-react-router'
import {
    MapDispatchToProps as _MapDispatchToProps,
    MapStateToProps as _MapStateToProps,
} from 'react-redux'

export type RootAction =
    | AppAction
    | LocaleAction
    | WebposAction
    | WebposIDAction

export interface RootState {
    readonly app: AppState
    readonly locale: LocaleState
    readonly webpos: WebposState
    readonly webposID: WebposIDState
    readonly router: RouterState
}

export type MapStateToProps<
    StateProps = object,
    OwnProps = object
> = _MapStateToProps<StateProps, OwnProps, RootState>

export type MapDispatchToProps<
    DispatchProps = object,
    OwnProps = object
> = _MapDispatchToProps<DispatchProps, OwnProps>
