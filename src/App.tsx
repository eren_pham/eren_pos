import history from '~/history'
import { selectors as appSelectors } from '~/modules/App'
import Routes from '~/routes'
import { configureStore } from '~/store'
import { ThemeProvider } from '~/styles/styled'
import { RootState } from '~/types/redux'
import { ConnectedRouter } from 'connected-react-router'
import React from 'react'
import { I18nextProvider } from 'react-i18next'
import { connect, Provider as StoreProvider } from 'react-redux'
import i18n from './i18n'
import { LoadingBar } from '~/components/atoms'

export const ConnectedThemeProvider = connect((state: RootState) => ({
    theme: appSelectors.currentThemeObject(state),
}))(({ theme, children }: any) => (
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
))

const store = configureStore()

const App = (): React.ReactElement => {
    return (
        <StoreProvider store={store}>
            <I18nextProvider i18n={i18n}>
                <ConnectedThemeProvider>
                    <ConnectedRouter history={history}>
                        {/* Suspense for react-i18next's hooks in Page component level */}
                        <React.Suspense fallback={<LoadingBar />}>
                            <Routes />
                        </React.Suspense>
                    </ConnectedRouter>
                </ConnectedThemeProvider>
            </I18nextProvider>
        </StoreProvider>
    )
}

export default App
