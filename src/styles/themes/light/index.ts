import { whiten } from '~/styles/colors'

export type Theme = typeof theme

const bodyBackground = '#eef4fb'
const primaryColor = '#1498d5'
const disableColor = 'rgba(0, 0, 0, 0.25)'

const white = '#fff'

const theme = {
    '@text-color': 'rgba(0, 0, 0, 0.65)',
    '@body-background': bodyBackground,
    '@box-shadow': '0 0 20px 0 rgba(139, 166, 193, 0.18)',
    '@border-color': '#dce2e9',
    '@border-radius': '2px',
    '@layout-header-background': white,
    '@disabled-color': disableColor,

    '@btn-primary-bg': primaryColor,
    '@btn-primary-border': whiten(primaryColor, 0.15),
    '@btn-primary-color': white,
}

export default theme
