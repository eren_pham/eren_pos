import lightTheme, { Theme as LightThemeType } from './light'

export type Theme = LightThemeType

export interface ThemeProps {
    readonly theme: Theme
}

export const themes = {
    light: (): any => lightTheme,
}

export type Themes = keyof typeof themes
