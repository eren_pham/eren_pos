'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
// All antd theme variables: https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
exports.default = {
    // '@light': '#fff',
    // '@dark': '#000',
    // '@body-background': '#f0f2f5',
    // '@layout-header-background': '@light',
    // '@primary-color': '#1498d5',
    '@primary-color': '#1DA57A',
}
