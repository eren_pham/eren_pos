export interface AntAttribute {
    readonly '@box-shadow': string
    readonly '@border-radius': string
    // -------- Colors -----------
    readonly '@primary-color': string
    readonly '@info-color': string
    readonly '@success-color': string
    readonly '@processing-color': string
    readonly '@error-color': string
    readonly '@highlight-color': string
    readonly '@warning-color': string
    readonly '@normal-color': string
    readonly '@white': string
    readonly '@black': string

    // Color used by default to control hover and active backgrounds and for
    // alert info backgrounds.
    readonly '@primary-1': string // replace tint(@primary-color, 90%)
    readonly '@primary-2': string // replace tint(@primary-color, 80%)
    readonly '@primary-3': string // unused
    readonly '@primary-4': string // unused
    readonly '@primary-5': string // color used to control the text color in many active and hover states, replace tint(@primary-color, 20%)
    readonly '@primary-6': string // color used to control the text color of active buttons, don't use, use @primary-color
    readonly '@primary-7': string // replace shade(@primary-color, 5%)
    readonly '@primary-8': string // unused
    readonly '@primary-9': string // unused
    readonly '@primary-10': string // unused

    // Background color for `<body>`
    readonly '@body-background': string
    // Base background color for most components
    readonly '@component-background': string
    // Popover background color
    readonly '@popover-background': string
    readonly '@popover-customize-border-color': string
    readonly '@font-family': string
    readonly '@code-family': string
    readonly '@text-color': string
    readonly '@text-color-secondary': string
    readonly '@text-color-inverse': string
    readonly '@icon-color': string
    readonly '@icon-color-hover': string
    readonly '@heading-color': string
    readonly '@heading-color-dark': string
    readonly '@text-color-dark': string
    readonly '@text-color-secondary-dark': string
    readonly '@text-selection-bg': string
    readonly '@font-variant-base': string
    readonly '@font-feature-settings-base': string
    readonly '@font-size-base': string
    readonly '@font-size-lg': string
    readonly '@font-size-sm': string
    readonly '@heading-1-size': string
    readonly '@heading-2-size': string
    readonly '@heading-3-size': string
    readonly '@heading-4-size': string
    // https://github.com/ant-design/ant-design/issues/20210
    readonly '@line-height-base': string
    readonly '@border-radius-base': string
    readonly '@border-radius-sm': string

    // vertical paddings
    readonly '@padding-lg': string // containers
    readonly '@padding-md': string // small containers and buttons
    readonly '@padding-sm': string // Form controls and items
    readonly '@padding-xs': string // small items
    readonly '@padding-xss': string // more small

    // vertical padding for all form controls
    readonly '@control-padding-horizontal': string
    readonly '@control-padding-horizontal-sm': string

    // vertical margins
    readonly '@margin-lg': string // containers
    readonly '@margin-md': string // small containers and buttons
    readonly '@margin-sm': string // Form controls and items
    readonly '@margin-xs': string // small items
    readonly '@margin-xss': string // more small

    // height rules
    readonly '@height-base': string
    readonly '@height-lg': string
    readonly '@height-sm': string

    // The background colors for active and hover states for things like
    // list items or table cells.
    readonly '@item-active-bg': string
    readonly '@item-hover-bg': string

    // ICONFONT
    readonly '@iconfont-css-prefix': string

    // LINK
    readonly '@link-color': string
    readonly '@link-hover-color': string
    readonly '@link-active-color': string
    readonly '@link-decoration': string
    readonly '@link-hover-decoration': string
    readonly '@link-focus-decoration': string
    readonly '@link-focus-outline': string

    // Animation
    readonly '@ease-base-out': string
    readonly '@ease-base-in': string
    readonly '@ease-out': string
    readonly '@ease-in': string
    readonly '@ease-in-out': string
    readonly '@ease-out-back': string
    readonly '@ease-in-back': string
    readonly '@ease-in-out-back': string
    readonly '@ease-out-circ': string
    readonly '@ease-in-circ': string
    readonly '@ease-in-out-circ': string
    readonly '@ease-out-quint': string
    readonly '@ease-in-quint': string
    readonly '@ease-in-out-quint': string

    // Border color
    readonly '@border-color-base': string // base border outline a component
    readonly '@border-color-split': string // split border inside a component
    readonly '@border-color-inverse': string
    readonly '@border-width-base': string // width of the border for a component
    readonly '@border-style-base': string // style of a components border

    // Outline
    readonly '@outline-blur-size': string
    readonly '@outline-width': string
    readonly '@outline-color': string
    readonly '@outline-fade': string

    readonly '@background-color-light': string // background of header and selected item
    readonly '@background-color-base': string // Default grey background color

    // Disabled states
    readonly '@disabled-color': string
    readonly '@disabled-bg': string
    readonly '@disabled-color-dark': string

    // Shadow
    readonly '@shadow-color': string
    readonly '@shadow-color-inverse': string
    readonly '@box-shadow-base': string
    readonly '@shadow-1-up': string
    readonly '@shadow-1-down': string
    readonly '@shadow-1-left': string
    readonly '@shadow-1-right': string
    readonly '@shadow-2': string

    // Buttons
    readonly '@btn-font-weight': string
    readonly '@btn-border-radius-base': string
    readonly '@btn-border-radius-sm': string
    readonly '@btn-border-width': string
    readonly '@btn-border-style': string
    readonly '@btn-shadow': string
    readonly '@btn-primary-shadow': string
    readonly '@btn-text-shadow': string

    readonly '@btn-primary-color': string
    readonly '@btn-primary-bg': string

    readonly '@btn-default-color': string
    readonly '@btn-default-bg': string
    readonly '@btn-default-border': string

    readonly '@btn-danger-color': string
    readonly '@btn-danger-bg': string
    readonly '@btn-danger-border': string

    readonly '@btn-disable-color': string
    readonly '@btn-disable-bg': string
    readonly '@btn-disable-border': string

    readonly '@btn-default-ghost-color': string
    readonly '@btn-default-ghost-bg': string
    readonly '@btn-default-ghost-border': string

    readonly '@btn-font-size-lg': string
    readonly '@btn-font-size-sm': string
    readonly '@btn-padding-horizontal-base': string
    readonly '@btn-padding-horizontal-lg': string
    readonly '@btn-padding-horizontal-sm': string

    readonly '@btn-height-base': string
    readonly '@btn-height-lg': string
    readonly '@btn-height-sm': string

    readonly '@btn-circle-size': string
    readonly '@btn-circle-size-lg': string
    readonly '@btn-circle-size-sm': string

    readonly '@btn-square-size': string
    readonly '@btn-square-size-lg': string
    readonly '@btn-square-size-sm': string
    readonly '@btn-square-only-icon-size': string
    readonly '@btn-square-only-icon-size-sm': string
    readonly '@btn-square-only-icon-size-lg': string

    readonly '@btn-group-border': string

    readonly '@btn-link-ghost-color': string

    readonly '@btn-ghost-color': string // new
    readonly '@btn-ghost-border': string // new

    readonly '@btn-primary-border': string // new
    readonly '@btn-dashed-bg': string // new
    readonly '@btn-dashed-border': string // new
    readonly '@btn-dashed-color': string // new
    readonly '@btn-link-bg': string // new
    readonly '@btn-link-border': string // new
    readonly '@btn-link-color': string // new

    // Checkbox
    readonly '@checkbox-size': string
    readonly '@checkbox-color': string
    readonly '@checkbox-check-color': string
    readonly '@checkbox-check-bg': string
    readonly '@checkbox-border-width': string

    readonly '@checkbox-border': string // new

    // Descriptions
    readonly '@descriptions-bg': string
    readonly '@descriptions-title-margin-bottom': string
    readonly '@descriptions-default-padding': string
    readonly '@descriptions-middle-padding': string
    readonly '@descriptions-small-padding': string

    // Dropdown
    readonly '@dropdown-selected-color': string
    readonly '@dropdown-menu-submenu-disabled-bg': string

    // Empty
    readonly '@empty-font-size': string

    // Radio
    readonly '@radio-size': string
    readonly '@radio-dot-color': string
    readonly '@radio-dot-disabled-color': string
    // solid text-color
    readonly '@radtio-solid-checked-color': string
    // Keep typo compatible
    readonly '@radio-solid-checked-color': string

    // Radio buttons
    readonly '@radio-button-bg': string
    readonly '@radio-button-checked-bg': string
    readonly '@radio-button-color': string
    readonly '@radio-button-hover-color': string
    readonly '@radio-button-active-color': string
    readonly '@radio-disabled-button-checked-bg': string
    readonly '@radio-disabled-button-checked-color': string

    readonly '@radio-button-checked-solid-bg': string // new
    readonly '@radio-button-color-disable': string // new
    readonly '@radio-button-border': string // new
    readonly '@radio-button-border-inner': string // new
    readonly '@radio-button-checked-border': string // new
    readonly '@radio-button-bg-disable': string // new
    readonly '@radio-button-border-disable': string // new

    // Media queries breakpoints
    // Extra small screen / phone
    readonly '@screen-xs': string
    readonly '@screen-xs-min': string

    // Small screen / tablet
    readonly '@screen-sm': string
    readonly '@screen-sm-min': string

    // Medium screen / desktop
    readonly '@screen-md': string
    readonly '@screen-md-min': string

    // Large screen / wide desktop
    readonly '@screen-lg': string
    readonly '@screen-lg-min': string

    // Extra large screen / full hd
    readonly '@screen-xl': string
    readonly '@screen-xl-min': string

    // Extra extra large screen / large desktop
    readonly '@screen-xxl': string
    readonly '@screen-xxl-min': string

    // provide a maximum
    readonly '@screen-xs-max': string
    readonly '@screen-sm-max': string
    readonly '@screen-md-max': string
    readonly '@screen-lg-max': string
    readonly '@screen-xl-max': string

    // Grid system
    readonly '@grid-columns': string
    readonly '@grid-gutter-width': string

    // Layout
    readonly '@layout-body-background': string
    readonly '@layout-header-background': string
    readonly '@layout-header-height': string
    readonly '@layout-header-padding': string
    readonly '@layout-header-color': string
    readonly '@layout-footer-padding': string
    readonly '@layout-footer-background': string
    readonly '@layout-sider-background': string
    readonly '@layout-trigger-height': string
    readonly '@layout-trigger-background': string
    readonly '@layout-trigger-color': string
    readonly '@layout-zero-trigger-width': string
    readonly '@layout-zero-trigger-height': string
    // Layout light theme
    readonly '@layout-sider-background-light': string
    readonly '@layout-trigger-background-light': string
    readonly '@layout-trigger-color-light': string

    // z-index list, order by `z-index`
    readonly '@zindex-badge': string
    readonly '@zindex-table-fixed': string
    readonly '@zindex-affix': string
    readonly '@zindex-back-top': string
    readonly '@zindex-picker-panel': string
    readonly '@zindex-popup-close': string
    readonly '@zindex-modal': string
    readonly '@zindex-modal-mask': string
    readonly '@zindex-message': string
    readonly '@zindex-notification': string
    readonly '@zindex-popover': string
    readonly '@zindex-dropdown': string
    readonly '@zindex-picker': string
    readonly '@zindex-tooltip': string

    // Animation
    readonly '@animation-duration-slow': string // Modal
    readonly '@animation-duration-base': string
    readonly '@animation-duration-fast': string // Tooltip

    //CollapsePanel
    readonly '@collapse-panel-border-radius': string

    //Dropdown
    readonly '@dropdown-menu-bg': string
    readonly '@dropdown-vertical-padding': string
    readonly '@dropdown-edge-child-vertical-padding': string
    readonly '@dropdown-font-size': string
    readonly '@dropdown-line-height': string

    // Form
    // ---
    readonly '@label-required-color': string
    readonly '@label-color': string
    readonly '@form-warning-input-bg': string
    readonly '@form-item-margin-bottom': string
    readonly '@form-item-trailing-colon': string
    readonly '@form-vertical-label-padding': string
    readonly '@form-vertical-label-margin': string
    readonly '@form-item-label-font-size': string
    readonly '@form-item-label-height': string
    readonly '@form-item-label-colon-margin-right': string
    readonly '@form-item-label-colon-margin-left': string
    readonly '@form-error-input-bg': string

    // Input
    // ---
    readonly '@input-height-base': string
    readonly '@input-height-lg': string
    readonly '@input-height-sm': string
    readonly '@input-padding-horizontal': string
    readonly '@input-padding-horizontal-base': string
    readonly '@input-padding-horizontal-sm': string
    readonly '@input-padding-horizontal-lg': string
    readonly '@input-padding-vertical-base': string
    readonly '@input-padding-vertical-sm': string
    readonly '@input-padding-vertical-lg': string
    readonly '@input-placeholder-color': string
    readonly '@input-color': string
    readonly '@input-icon-color': string
    readonly '@input-border-color': string
    readonly '@input-border-shadow-color': string // new
    readonly '@input-bg': string
    readonly '@input-number-hover-border-color': string
    readonly '@input-number-handler-active-bg': string
    readonly '@input-number-handler-hover-bg': string
    readonly '@input-number-handler-bg': string
    readonly '@input-number-handler-border-color': string
    readonly '@input-addon-bg': string
    readonly '@input-hover-border-color': string
    readonly '@input-disabled-bg': string
    readonly '@input-outline-offset': string
    readonly '@input-icon-hover-color': string

    // Mentions
    // ---
    readonly '@mentions-dropdown-bg': string
    readonly '@mentions-dropdown-menu-item-hover-bg': string

    // Select
    // ---
    readonly '@select-border-color': string
    readonly '@select-item-selected-font-weight': string
    readonly '@select-dropdown-bg': string
    readonly '@select-item-selected-bg': string
    readonly '@select-item-active-bg': string
    readonly '@select-dropdown-vertical-padding': string
    readonly '@select-dropdown-font-size': string
    readonly '@select-dropdown-line-height': string
    readonly '@select-dropdown-height': string
    readonly '@select-background': string
    readonly '@select-clear-background': string
    readonly '@select-selection-item-bg': string
    readonly '@select-selection-item-border-color': string
    readonly '@select-single-item-height-lg': string
    readonly '@select-multiple-item-height': string // Normal 24px
    readonly '@select-multiple-item-height-lg': string
    readonly '@select-multiple-item-spacing-half': string

    readonly '@select-item-hover-bg': string // new
    readonly '@select-border-hover': string // new

    // Cascader
    // ---
    readonly '@cascader-bg': string
    readonly '@cascader-item-selected-bg': string
    readonly '@cascader-menu-bg': string
    readonly '@cascader-menu-border-color-split': string

    // Cascader
    // ----
    readonly '@cascader-dropdown-vertical-padding': string
    readonly '@cascader-dropdown-edge-child-vertical-padding': string
    readonly '@cascader-dropdown-font-size': string
    readonly '@cascader-dropdown-line-height': string

    // Anchor
    // ---
    readonly '@anchor-bg': string
    readonly '@anchor-border-color': string

    // Tooltip
    // ---
    // Tooltip max width
    readonly '@tooltip-max-width': string
    // Tooltip text color
    readonly '@tooltip-color': string
    // Tooltip background color
    readonly '@tooltip-bg': string
    // Tooltip arrow width
    readonly '@tooltip-arrow-width': string
    // Tooltip distance with trigger
    readonly '@tooltip-distance': string
    // Tooltip arrow color
    readonly '@tooltip-arrow-color': string

    // Popover
    // ---
    // Popover body background color
    readonly '@popover-bg': string
    // Popover text color
    readonly '@popover-color': string
    // Popover maximum width
    readonly '@popover-min-width': string
    readonly '@popover-min-height': string
    // Popover arrow width
    readonly '@popover-arrow-width': string
    // Popover arrow color
    readonly '@popover-arrow-color': string
    // Popover outer arrow width
    // Popover outer arrow color
    readonly '@popover-arrow-outer-color': string
    // Popover distance with trigger
    readonly '@popover-distance': string
    readonly '@popover-padding-horizontal': string

    // Modal
    // --
    readonly '@modal-body-padding': string
    readonly '@modal-header-bg': string
    readonly '@modal-header-padding': string
    readonly '@modal-header-border-color-split': string
    readonly '@modal-header-close-size': string
    readonly '@modal-content-bg': string
    readonly '@modal-heading-color': string
    readonly '@modal-footer-bg': string
    readonly '@modal-footer-border-color-split': string
    readonly '@modal-footer-padding-vertical': string
    readonly '@modal-footer-padding-horizontal': string
    readonly '@modal-mask-bg': string
    readonly '@modal-confirm-body-padding': string

    // Progress
    // --
    readonly '@progress-default-color': string
    readonly '@progress-remaining-color': string
    readonly '@progress-text-color': string
    readonly '@progress-radius': string
    readonly '@progress-steps-item-bg': string

    // Menu
    // ---
    readonly '@menu-inline-toplevel-item-height': string
    readonly '@menu-item-height': string
    readonly '@menu-item-group-height': string
    readonly '@menu-collapsed-width': string
    readonly '@menu-bg': string
    readonly '@menu-popup-bg': string
    readonly '@menu-item-color': string
    readonly '@menu-highlight-color': string
    readonly '@menu-item-active-bg': string
    readonly '@menu-item-active-border-width': string
    readonly '@menu-item-group-title-color': string
    readonly '@menu-icon-size': string
    readonly '@menu-icon-size-lg': string

    readonly '@menu-item-vertical-margin': string
    readonly '@menu-item-font-size': string
    readonly '@menu-item-boundary-margin': string

    readonly '@menu-sub-hover-bg': string // new
    readonly '@menu-disable-color': string // new

    // dark theme
    readonly '@menu-dark-color': string
    readonly '@menu-dark-bg': string
    readonly '@menu-dark-arrow-color': string
    readonly '@menu-dark-submenu-bg': string
    readonly '@menu-dark-highlight-color': string
    readonly '@menu-dark-item-active-bg': string
    readonly '@menu-dark-selected-item-icon-color': string
    readonly '@menu-dark-selected-item-text-color': string
    readonly '@menu-dark-item-hover-bg': string
    // Spin
    // ---
    readonly '@spin-dot-size-sm': string
    readonly '@spin-dot-size': string
    readonly '@spin-dot-size-lg': string

    // Table
    // --
    readonly '@table-bg': string
    readonly '@table-header-bg': string
    readonly '@table-header-color': string
    readonly '@table-header-sort-bg': string
    readonly '@table-body-sort-bg': string
    readonly '@table-row-hover-bg': string
    readonly '@table-selected-row-color': string
    readonly '@table-selected-row-bg': string
    readonly '@table-body-selected-sort-bg': string
    readonly '@table-selected-row-hover-bg': string
    readonly '@table-expanded-row-bg': string
    readonly '@table-padding-vertical': string
    readonly '@table-padding-horizontal': string
    readonly '@table-padding-vertical-md': string
    readonly '@table-padding-horizontal-md': string
    readonly '@table-padding-vertical-sm': string
    readonly '@table-padding-horizontal-sm': string
    readonly '@table-border-radius-base': string
    readonly '@table-footer-bg': string
    readonly '@table-footer-color': string
    readonly '@table-header-bg-sm': string
    // Sorter
    // Legacy: `table-header-sort-active-bg` is used for hover not real active
    readonly '@table-header-sort-active-bg': string
    // Filter
    readonly '@table-header-filter-active-bg': string
    readonly '@table-filter-btns-bg': string
    readonly '@table-filter-dropdown-bg': string
    readonly '@table-expand-icon-bg': string

    // Tag
    // --
    readonly '@tag-default-bg': string
    readonly '@tag-default-color': string
    readonly '@tag-font-size': string

    // TimePicker
    // ---
    readonly '@picker-bg': string
    readonly '@picker-basic-cell-hover-color': string
    readonly '@picker-basic-cell-active-with-range-color': string
    readonly '@picker-basic-cell-hover-with-range-color': string
    readonly '@picker-basic-cell-disabled-bg': string
    readonly '@picker-border-color': string
    readonly '@picker-date-hover-range-border-color': string
    readonly '@picker-date-hover-range-color': string
    readonly '@picker-time-panel-cell-height': string

    // Calendar
    // ---
    readonly '@calendar-bg': string
    readonly '@calendar-input-bg': string
    readonly '@calendar-border-color': string
    readonly '@calendar-item-active-bg': string
    readonly '@calendar-full-bg': string
    readonly '@calendar-full-panel-bg': string

    // Carousel
    // ---
    readonly '@carousel-dot-width': string
    readonly '@carousel-dot-height': string
    readonly '@carousel-dot-active-width': string

    // Badge
    // ---
    readonly '@badge-height': string
    readonly '@badge-dot-size': string
    readonly '@badge-font-size': string
    readonly '@badge-font-weight': string
    readonly '@badge-status-size': string
    readonly '@badge-text-color': string

    // Rate
    // ---
    readonly '@rate-star-color': string
    readonly '@rate-star-bg': string

    // Card
    // ---
    readonly '@card-head-color': string
    readonly '@card-head-background': string
    readonly '@card-head-font-size': string
    readonly '@card-head-font-size-sm': string
    readonly '@card-head-padding': string
    readonly '@card-head-padding-sm': string
    readonly '@card-head-height': string
    readonly '@card-head-height-sm': string
    readonly '@card-inner-head-padding': string
    readonly '@card-padding-base': string
    readonly '@card-padding-base-sm': string
    readonly '@card-actions-background': string
    readonly '@card-actions-li-margin': string
    readonly '@card-skeleton-bg': string
    readonly '@card-background': string
    readonly '@card-shadow': string
    readonly '@card-radius': string
    readonly '@card-head-tabs-margin-bottom': string

    // Comment
    // ---
    readonly '@comment-bg': string
    readonly '@comment-padding-base': string
    readonly '@comment-nest-indent': string
    readonly '@comment-font-size-base': string
    readonly '@comment-font-size-sm': string
    readonly '@comment-author-name-color': string
    readonly '@comment-author-time-color': string
    readonly '@comment-action-color': string
    readonly '@comment-action-hover-color': string
    readonly '@comment-actions-margin-bottom': string
    readonly '@comment-actions-margin-top': string
    readonly '@comment-content-detail-p-margin-bottom': string

    // Tabs
    // ---
    readonly '@tabs-card-head-background': string
    readonly '@tabs-card-height': string
    readonly '@tabs-card-active-color': string
    readonly '@tabs-card-horizontal-padding': string
    readonly '@tabs-card-horizontal-padding-sm': string
    readonly '@tabs-title-font-size': string
    readonly '@tabs-title-font-size-lg': string
    readonly '@tabs-title-font-size-sm': string
    readonly '@tabs-ink-bar-color': string
    readonly '@tabs-bar-margin': string
    readonly '@tabs-horizontal-margin': string
    readonly '@tabs-horizontal-margin-rtl': string
    readonly '@tabs-horizontal-padding': string
    readonly '@tabs-horizontal-padding-lg': string
    readonly '@tabs-horizontal-padding-sm': string
    readonly '@tabs-vertical-padding': string
    readonly '@tabs-vertical-margin': string
    readonly '@tabs-scrolling-size': string
    readonly '@tabs-highlight-color': string
    readonly '@tabs-hover-color': string
    readonly '@tabs-active-color': string
    readonly '@tabs-card-gutter': string
    readonly '@tabs-card-tab-active-border-top': string

    // BackTop
    // ---
    readonly '@back-top-color': string
    readonly '@back-top-bg': string
    readonly '@back-top-hover-bg': string

    // Avatar
    // ---
    readonly '@avatar-size-base': string
    readonly '@avatar-size-lg': string
    readonly '@avatar-size-sm': string
    readonly '@avatar-font-size-base': string
    readonly '@avatar-font-size-lg': string
    readonly '@avatar-font-size-sm': string
    readonly '@avatar-bg': string
    readonly '@avatar-color': string
    readonly '@avatar-border-radius': string

    // Switch
    // ---
    readonly '@switch-height': string
    readonly '@switch-sm-height': string
    readonly '@switch-min-width': string
    readonly '@switch-sm-min-width': string
    readonly '@switch-sm-checked-margin-left': string
    readonly '@switch-disabled-opacity': string
    readonly '@switch-color': string
    readonly '@switch-bg': string
    readonly '@switch-shadow-color': string

    // Pagination
    // ---
    readonly '@pagination-item-bg': string
    readonly '@pagination-item-size': string
    readonly '@pagination-item-size-sm': string
    readonly '@pagination-font-family': string
    readonly '@pagination-font-weight-active': string
    readonly '@pagination-item-bg-active': string
    readonly '@pagination-item-link-bg': string
    readonly '@pagination-item-disabled-color-active': string
    readonly '@pagination-item-disabled-bg-active': string
    readonly '@pagination-item-input-bg': string
    readonly '@pagination-mini-options-size-changer-top': string

    // PageHeader
    // ---
    readonly '@page-header-padding': string
    readonly '@page-header-padding-vertical': string
    readonly '@page-header-padding-breadcrumb': string
    readonly '@page-header-content-padding-vertical': string
    readonly '@page-header-back-color': string
    readonly '@page-header-ghost-bg': string

    // Breadcrumb
    // ---
    readonly '@breadcrumb-base-color': string
    readonly '@breadcrumb-last-item-color': string
    readonly '@breadcrumb-font-size': string
    readonly '@breadcrumb-icon-font-size': string
    readonly '@breadcrumb-link-color': string
    readonly '@breadcrumb-link-color-hover': string
    readonly '@breadcrumb-separator-color': string
    readonly '@breadcrumb-separator-margin': string

    // Slider
    // ---
    readonly '@slider-margin': string
    readonly '@slider-rail-background-color': string
    readonly '@slider-rail-background-color-hover': string
    readonly '@slider-track-background-color': string
    readonly '@slider-track-background-color-hover': string
    readonly '@slider-handle-border-width': string
    readonly '@slider-handle-background-color': string
    readonly '@slider-handle-color': string
    readonly '@slider-handle-color-hover': string
    readonly '@slider-handle-color-focus': string
    readonly '@slider-handle-color-focus-shadow': string
    readonly '@slider-handle-color-tooltip-open': string
    readonly '@slider-handle-shadow': string
    readonly '@slider-dot-border-color': string
    readonly '@slider-dot-border-color-active': string
    readonly '@slider-disabled-color': string
    readonly '@slider-disabled-background-color': string

    // Tree
    // ---
    readonly '@tree-bg': string
    readonly '@tree-title-height': string
    readonly '@tree-child-padding': string
    readonly '@tree-directory-selected-color': string
    readonly '@tree-directory-selected-bg': string
    readonly '@tree-node-hover-bg': string
    readonly '@tree-node-selected-bg': string

    // Collapse
    // ---
    readonly '@collapse-header-padding': string
    readonly '@collapse-header-padding-extra': string
    readonly '@collapse-header-bg': string
    readonly '@collapse-content-padding': string
    readonly '@collapse-content-bg': string

    // Skeleton
    // ---
    readonly '@skeleton-color': string
    readonly '@skeleton-to-color': string
    readonly '@skeleton-paragraph-margin-top': string
    readonly '@skeleton-paragraph-li-margin-top': string

    // Transfer
    // ---
    readonly '@transfer-header-height': string
    readonly '@transfer-item-height': string
    readonly '@transfer-disabled-bg': string
    readonly '@transfer-list-height': string
    readonly '@transfer-item-hover-bg': string
    readonly '@transfer-item-padding-vertical': string
    readonly '@transfer-list-search-icon-top': string

    // Message
    // ---
    readonly '@message-notice-content-padding': string
    readonly '@message-notice-content-bg': string
    // Motion
    // ---
    readonly '@wave-animation-width': string

    // Alert
    // ---
    readonly '@alert-success-border-color': string
    readonly '@alert-success-bg-color': string
    readonly '@alert-success-icon-color': string
    readonly '@alert-info-border-color': string
    readonly '@alert-info-bg-color': string
    readonly '@alert-info-icon-color': string
    readonly '@alert-warning-border-color': string
    readonly '@alert-warning-bg-color': string
    readonly '@alert-warning-icon-color': string
    readonly '@alert-error-border-color': string
    readonly '@alert-error-bg-color': string
    readonly '@alert-error-icon-color': string
    readonly '@alert-message-color': string
    readonly '@alert-text-color': string
    readonly '@alert-close-color': string
    readonly '@alert-close-hover-color': string
    readonly '@alert-no-icon-padding-vertical': string
    readonly '@alert-with-description-no-icon-padding-vertical': string

    // List
    // ---
    readonly '@list-header-background': string
    readonly '@list-footer-background': string
    readonly '@list-empty-text-padding': string
    readonly '@list-item-padding': string
    readonly '@list-item-padding-sm': string
    readonly '@list-item-padding-lg': string
    readonly '@list-item-meta-margin-bottom': string
    readonly '@list-item-meta-avatar-margin-right': string
    readonly '@list-item-meta-title-margin-bottom': string
    readonly '@list-customize-card-bg': string
    readonly '@list-item-meta-description-font-size': string

    // Statistic
    // ---
    readonly '@statistic-title-font-size': string
    readonly '@statistic-content-font-size': string
    readonly '@statistic-unit-font-size': string
    readonly '@statistic-font-family': string

    // Drawer
    // ---
    readonly '@drawer-header-padding': string
    readonly '@drawer-body-padding': string
    readonly '@drawer-bg': string
    readonly '@drawer-footer-padding-vertical': string
    readonly '@drawer-footer-padding-horizontal': string
    readonly '@drawer-header-close-size': string

    // Timeline
    // ---
    readonly '@timeline-width': string
    readonly '@timeline-color': string
    readonly '@timeline-dot-border-width': string
    readonly '@timeline-dot-color': string
    readonly '@timeline-dot-bg': string
    readonly '@timeline-item-padding-bottom': string

    // Typography
    // ---
    readonly '@typography-title-font-weight': string
    readonly '@typography-title-margin-top': string
    readonly '@typography-title-margin-bottom': string

    // Upload
    // ---
    readonly '@upload-actions-color': string

    // Steps
    // ---
    readonly '@process-tail-color': string
    readonly '@steps-nav-arrow-color': string
    readonly '@steps-background': string
    readonly '@steps-icon-size': string
    readonly '@steps-icon-custom-size': string
    readonly '@steps-icon-custom-top': string
    readonly '@steps-icon-custom-font-size': string
    readonly '@steps-icon-top': string
    readonly '@steps-icon-font-size': string
    readonly '@steps-icon-margin': string
    readonly '@steps-title-line-height': string
    readonly '@steps-small-icon-size': string
    readonly '@steps-small-icon-margin': string
    readonly '@steps-dot-size': string
    readonly '@steps-dot-top': string
    readonly '@steps-current-dot-size': string
    readonly '@steps-desciption-max-width': string
    readonly '@steps-nav-content-max-width': string

    // Notification
    readonly '@notification-bg': string
    readonly '@notification-shadow': string // new

    readonly '@hover-color': string
    readonly '@border-color': string
    readonly '@light': string
    readonly '@dark': string

    readonly '@card-hover-border': string
    readonly '@tail-color': string

    readonly '@anchor-border-width': string
    readonly '@site-text-color': string
    readonly '@site-border-color-split': string
    readonly '@site-heading-color': string
    readonly '@site-header-box-shadow': string
    readonly '@home-text-color': string
    readonly '@gray-8': string
    readonly '@pro-header-box-shadow': string
}
