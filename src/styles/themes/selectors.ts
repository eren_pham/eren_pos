import { AntAttribute } from '~/styles/themes/ant/AntAttribute'
import theme from 'styled-theming'

export const getColor = (key: keyof AntAttribute) => ({ theme }: any): string =>
    (theme as any)[key] || ''

export const getThemeColor = (key: keyof AntAttribute) =>
    theme('mode', {
        light: getColor(key),
        dark: getColor(key),
    })
