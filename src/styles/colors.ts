import Color from 'color'

export const withAlpha = (color: string, alpha: number) =>
    Color(color).alpha(alpha).toString()

export const whiten = (color: string, by: number) =>
    Color(color).lighten(by).hex().toString()

export const blacken = (color: string, by: number) =>
    Color(color).darken(by).toString()

export const isColorLight = (color: string): boolean => {
    return Color(color).isLight()
}

export const stringToColor = (str: string): string => {
    let hash = 0

    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash)
    }

    let color = '#'

    for (let i = 0; i < 3; i++) {
        const value = (hash >> (i * 8)) & 0xff
        color += ('00' + value.toString(16)).substr(-2)
    }

    return color
}
