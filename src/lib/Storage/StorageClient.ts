export default abstract class StorageClient<T = any> {
    private storage: T | null = null

    public abstract get(key: string): any
    public abstract set(key: string, value: any): void
    public abstract remove(key: string): void
    public abstract clear(): void

    public setStorage(storage: T): void {
        this.storage = storage
    }

    public getStorage(): T | null {
        return this.storage
    }
}
