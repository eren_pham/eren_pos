import StorageClient from './StorageClient'

class LocalStorageClient extends StorageClient {
    constructor() {
        super()
        if (typeof window !== 'undefined') {
            this.setStorage(window.localStorage)
        }
    }

    public setStorage(storage: any): void {
        super.setStorage(storage)
    }

    public set(key: string, value: any) {
        return this.getStorage().setItem(key, JSON.stringify(value))
    }

    public get<T = any>(key: string) {
        try {
            const value = this.getStorage().getItem(key)
            return JSON.parse(value) as T
        } catch (e) {}
        return null
    }

    public remove(key: string) {
        return this.getStorage().removeItem(key)
    }

    get length(): number {
        return this.getStorage().length
    }

    public clear() {
        return this.getStorage().clear()
    }
}

export default LocalStorageClient
