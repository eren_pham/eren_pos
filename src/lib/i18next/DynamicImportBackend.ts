interface IDynamicImportBackendOptions {
    readonly loaders: { readonly [lang: string]: any }
}

export default class DynamicImportBackend {
    public static readonly type = 'backend'

    public readonly type = DynamicImportBackend.type

    public services: any

    public options: IDynamicImportBackendOptions = {
        loaders: {},
    }

    public logger: any = null

    public init(services: any, options = {}): void {
        this.services = services

        this.options = { ...(this.options || {}), ...options }

        if (this.services.logger) {
            this.logger = this.services.logger.create('dynamicImportBackend')
        }
    }

    public log(...args: string[]): void {
        if (this.logger) {
            this.logger.log(...args)
        }
    }

    public read(
        lang: string,
        ns: string,
        cb: (e: null | Error, data?: any) => any,
    ): any {
        this.log(`Loading lang resource for ${ns}: ${lang}`)

        const langLoaders = this.options.loaders[lang]
        if (!langLoaders || typeof langLoaders !== 'object') {
            return cb(new Error(`Cannot find loaders for lang: ${lang}`))
        }

        const loader = langLoaders[ns]
        if (!loader || typeof loader !== 'function') {
            return cb(
                new Error(
                    `Cannot find loader for namespace ${ns} for lang: ${lang}`,
                ),
            )
        }

        return Promise.resolve(loader())
            .then((data) => {
                this.log('Loaded lang resource:', lang, ns, data)
                cb(null, data)
            })
            .catch((e) => cb(e))
    }
}
