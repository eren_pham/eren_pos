import qs from 'qs'

type conditionType =
    | 'eq' // Equals.
    | 'finset' // A value within a set of values
    | 'from' // The beginning of a range. Must be used with to
    | 'gt' // Greater than
    | 'gteq' // Greater than or equal
    | 'in' // In. The value can contain a comma-separated list of values.
    | 'like' // Like. The value can contain the SQL wildcard characters when like is specified.
    | 'lt' // Less than
    | 'lteq' // Less than or equal
    | 'moreq' // More or equal
    | 'neq' // Not equal
    | 'nfinset' // A value that is not within a set of values
    | 'nin' // Not in. The value can contain a comma-separated list of values.
    | 'notnull' // Not null
    | 'null' // Null
    | 'to' // The end of a range. Must be used with from

type directionType = 'ASC' | 'DESC'

interface sortOrderItemInterface {
    field: string
    direction?: directionType
}

interface filterItemInterface {
    field: string // field is an attribute name.
    value: string // value specifies the value to search for.
    condition_type: conditionType
}

interface filtersInterface {
    filters: filterItemInterface[]
}

interface SearchCriteriaInterface {
    filter_groups?: filtersInterface[]
    sort_orders?: sortOrderItemInterface[]
    pageSize: number
    currentPage: number
}

export const addFilterItem = (
    field: string,
    value: string,
    condition_type: conditionType = 'eq',
): filterItemInterface => ({
    field,
    value,
    condition_type,
})
export const addSortOrderItem = (
    field: string,
    direction?: directionType,
): sortOrderItemInterface => ({
    field,
    direction,
})

export const addFilters = (
    ...filters: filterItemInterface[]
): filtersInterface => ({
    filters,
})

export const addFilterGroup = (...filter_groups: filtersInterface[]) =>
    filter_groups

export const addSortOrder = (...sort_orders: sortOrderItemInterface[]) =>
    sort_orders

export const searchCriteriaBuilder = (
    pageSize: number,
    currentPage: number,
    filter_groups: filtersInterface[],
    sort_orders?: sortOrderItemInterface[],
) =>
    qs.stringify(
        {
            searchCriteria: {
                filter_groups,
                sort_orders,
                pageSize,
                currentPage,
            },
        },
        { arrayFormat: 'indices' },
    )

// export const searchCriteria = () => {
//     const data = searchCriteriaBuilder(
//         16,
//         1,
//         addFilterGroup(
//             addFilters(
//                 addFilterItem('sku', 'WSH%29%', 'like'),
//                 addFilterItem('sku', 'WP%29%', 'like'),
//             ),
//             addFilters(addFilterItem('price', '40', 'from')),
//             addFilters(addFilterItem('price', '49.99', 'to')),
//         ),
//         addSortOrder(addSortOrderItem('price', 'ASC')),
//     )
//     console.log(data)
// }
