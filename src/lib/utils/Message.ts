import { message } from 'antd'

type TypeMessage = 'success' | 'error' | 'warning'

export default (
    text: string,
    type: TypeMessage = 'success',
    duration?: number,
) => message[type](text, duration)
