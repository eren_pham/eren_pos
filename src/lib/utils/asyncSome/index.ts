async function asyncSome<V>(
    list: ReadonlyArray<V>,
    tester: (item: V, index: number) => Promise<any>,
): Promise<V> {
    for (let i = 0; i < list.length; i++) {
        try {
            await tester(list[i], i)
            return Promise.resolve(list[i])
        } catch (e) {
            continue
        }
    }
    return Promise.reject()
}

export default asyncSome
