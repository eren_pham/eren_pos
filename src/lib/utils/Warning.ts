import { Modal } from 'antd'

export default (err: any) =>
    Modal.warning({
        title: err.response.message,
        content: 'Error code: ' + err.response.code,
    })
