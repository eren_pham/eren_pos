import { routerMiddleware } from 'connected-react-router'
import history from './history'
import { applyMiddleware, compose, createStore } from 'redux'
import rootReducer from './rootReducer'
import rootEpic from '~/rootEpic'
import { createEpicMiddleware } from 'redux-observable'

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: Function
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const configureStore = () => {
    const epicMiddleware = createEpicMiddleware<any>()

    const middlewares: ReadonlyArray<any> = [
        routerMiddleware(history),
        epicMiddleware,
    ]

    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(...middlewares)),
    )
    epicMiddleware.run(rootEpic)

    return store
}
