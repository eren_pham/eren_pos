import * as templates from '~/components/templates'
import { RouteConfig } from 'react-router-config'

import * as pages from './pageLoaders'
import routeNames from '~/routes/routeNames'

const NotFoundRoute = {
    component: pages.NotFound,
    path: '*',
}

const routes: RouteConfig[] = [
    {
        component: templates.Root,
        routes: [
            {
                path: routeNames.AUTH,
                component: templates.Auth,
                routes: [
                    {
                        path: routeNames.LOGIN,
                        exact: true,
                        component: pages.Login,
                    },
                    {
                        path: routeNames.ASSIGN_POS,
                        exact: true,
                        component: pages.AssignPOS,
                    },
                    NotFoundRoute,
                ],
            },
            {
                path: routeNames.HOME,
                component: templates.App,
                routes: [
                    {
                        path: routeNames.HOME,
                        exact: true,
                        component: pages.Home,
                    },
                    {
                        path: routeNames.ORDER,
                        exact: true,
                        component: pages.Order,
                    },
                    NotFoundRoute,
                ],
            },
            NotFoundRoute,
        ],
    },
]

export default routes
