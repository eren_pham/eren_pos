export default {
    HOME: '/',
    AUTH: '/auth',
    LOGIN: '/auth/login',
    ASSIGN_POS: '/auth/assign',
    ORDER: '/order',
}
