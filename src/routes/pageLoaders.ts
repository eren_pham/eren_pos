import React, { lazy } from 'react'

export const Home: React.ComponentType<any> = lazy(() =>
    import('~/components/pages/Home'),
)

export const Order: React.ComponentType<any> = lazy(() =>
    import('~/components/pages/Order'),
)

export const Login: React.ComponentType<any> = lazy(() =>
    import('~/components/pages/Login'),
)

export const AssignPOS: React.ComponentType<any> = lazy(() =>
    import('~/components/pages/AssignPOS'),
)

export const NotFound: React.ComponentType<any> = lazy(() =>
    import('~/components/pages/NotFound'),
)
