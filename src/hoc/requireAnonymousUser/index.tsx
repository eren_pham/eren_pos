import { push, replace } from 'connected-react-router'
import * as React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { selectors as webposIDSelectors } from '~/modules/WebposID'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'

export interface StateProps {
    readonly isSignedIn: boolean
    readonly isProcessing: boolean
}

export interface DispatchProps {
    readonly navigateTo: (location: string) => void
}

export type RequireAnonymousProps = StateProps & DispatchProps

const redirectByAuth = <P extends {}>(
    Component: React.ComponentType<P>,
): React.FC<P & RequireAnonymousProps> => ({
    isSignedIn,
    isProcessing,
    navigateTo,
    ...props
}: RequireAnonymousProps) => {
    React.useEffect(() => {
        if (isSignedIn && !isProcessing) {
            navigateTo('/')
        }
    }, [navigateTo, isProcessing, isSignedIn])
    return <Component {...(props as P)} />
}

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    isSignedIn: webposIDSelectors.isSignedIn(state),
    isProcessing: webposIDSelectors.isProcessing(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    navigateTo: (location: string) => dispatch(push(location)),
})

const requireAnonymousUser = <P extends {}>() =>
    compose<RequireAnonymousProps, P>(
        connect(mapStateToProps, mapDispatchToProps),
        redirectByAuth,
    )

export default requireAnonymousUser
