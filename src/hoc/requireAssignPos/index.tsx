import * as React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { PosInterface, selectors as webposSelectors } from '~/modules/Webpos'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import routeNames from '~/routes/routeNames'
import { replace } from 'connected-react-router'

export interface StateProps {
    readonly isAssignPos: PosInterface | null
}

export interface DispatchProps {
    readonly navigateTo: (location: string) => void
}

export type RequireSignedInProps = StateProps & DispatchProps

const redirectByAuth = <P extends {}>(
    Component: React.ComponentType<P>,
): React.FC<P & RequireSignedInProps> => ({
    isAssignPos,
    navigateTo,
    ...props
}: RequireSignedInProps) => {
    React.useEffect(() => {
        if (!isAssignPos) {
            navigateTo(routeNames.ASSIGN_POS)
        }
    }, [navigateTo, isAssignPos])

    return isAssignPos ? <Component {...(props as P)} /> : null
}

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    isAssignPos: webposSelectors.pos(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    navigateTo: (location: string) => dispatch(replace(location)),
})

const requireAssignPos = <P extends {}>() =>
    compose<RequireSignedInProps, P>(
        connect(mapStateToProps, mapDispatchToProps),
        redirectByAuth,
    )

export default requireAssignPos
