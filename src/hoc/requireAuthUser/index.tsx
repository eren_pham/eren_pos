import * as React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { selectors as webposIDSelectors } from '~/modules/WebposID'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import routeNames from '~/routes/routeNames'
import { replace } from 'connected-react-router'

export interface StateProps {
    readonly isSignedIn: boolean
    readonly isSignedOut: boolean
    readonly isProcessing: boolean
}

export interface DispatchProps {
    readonly navigateTo: (location: string) => void
}

export type RequireSignedInProps = StateProps & DispatchProps

const redirectByAuth = <P extends {}>(
    Component: React.ComponentType<P>,
): React.FC<P & RequireSignedInProps> => ({
    isSignedIn,
    isSignedOut,
    isProcessing,
    navigateTo,
    ...props
}: RequireSignedInProps) => {
    React.useEffect(() => {
        if (isSignedOut && !isProcessing) {
            navigateTo(routeNames.LOGIN)
        }
    }, [navigateTo, isProcessing, isSignedOut])

    return isSignedIn ? <Component {...(props as P)} /> : null
}

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    isSignedIn: webposIDSelectors.isSignedIn(state),
    isSignedOut: webposIDSelectors.isSignedOut(state),
    isProcessing: webposIDSelectors.isProcessing(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    navigateTo: (location: string) => dispatch(replace(location)),
})

const requireAuthUser = <P extends {}>() =>
    compose<RequireSignedInProps, P>(
        connect(mapStateToProps, mapDispatchToProps),
        redirectByAuth,
    )

export default requireAuthUser
