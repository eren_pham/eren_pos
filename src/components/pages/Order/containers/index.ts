import { withTranslation } from 'react-i18next'
import mapToProps from './mapToProps'
import requireAuthUser from '~/hoc/requireAuthUser'
import requireAssignPos from '~/hoc/requireAssignPos'

export default [
    requireAuthUser(),
    requireAssignPos(),
    mapToProps,
    withTranslation(),
]
