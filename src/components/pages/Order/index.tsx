import { compose } from 'recompose'
import containers from './containers'
import { Props } from './types'
import OrderPage from './views'

export default compose<Props, {}>(...containers)(OrderPage)
