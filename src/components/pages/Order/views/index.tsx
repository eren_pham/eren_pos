import React from 'react'
import { OrderDetail, OrderList } from '~/components/organisms'

const Order = (props: any) => {
    return (
        <>
            <OrderDetail />
            <OrderList />
        </>
    )
}

export default Order
