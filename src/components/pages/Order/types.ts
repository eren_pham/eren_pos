import { RouteConfigComponentProps } from 'react-router-config'
import { WithTranslation } from 'react-i18next'
import { InformationInterface } from '~/modules/Webpos'
import { UserLoginForm } from '~/modules/WebposID'

export interface StateProps {
    readonly websiteInformation: InformationInterface
    readonly isProcessing: boolean
}

export interface DispatchProps {
    readonly userClickLogin: (formData: UserLoginForm) => void
    readonly navigateTo: (
        routeName: string,
        isReplace?: boolean,
        params?: any,
    ) => void
}

export interface HandlerProps {}

export type Props = StateProps &
    DispatchProps &
    HandlerProps &
    RouteConfigComponentProps &
    WithTranslation
