import { compose } from 'recompose'
import containers from './containers'
import { Props } from './types'
import HomePage from './views'

export default compose<Props, {}>(...containers)(HomePage)
