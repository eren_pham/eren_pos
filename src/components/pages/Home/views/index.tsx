import React from 'react'
import { Cart, ProductList } from '~/components/organisms'

const Home = (props: any) => {
    return (
        <>
            <ProductList />
            <Cart />
        </>
    )
}

export default Home
