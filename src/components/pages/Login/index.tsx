import { compose } from 'recompose'
import containers from './containers'
import { Props } from './types'
import LoginPage from './views'

export default compose<Props, {}>(...containers)(LoginPage)
