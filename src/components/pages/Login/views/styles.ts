import styled from '~/styles/styled'
import { getBoxShadow, getBorderRadius } from './themes'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    bottom: 80px;
    margin: 0;
    padding: 0;
    height: 100vh;
    width: 100vw;
`

export const BoxLogin = styled.div`
    display: block;
    padding: 30px;
    width: 470px;
    background: #fff;
    border-radius: ${getBorderRadius};
    box-shadow: ${getBoxShadow};
`

export const Logo = styled.img`
    width: 100%;
`

export const LogoBox = styled.div`
    text-align: center;
    width: 250px;
    height: 100px;
    margin: 25px auto 50px;
`
