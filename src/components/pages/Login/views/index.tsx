import React, { useCallback } from 'react'
import { Props } from '../types'
import { BoxLogin, Container, Logo, LogoBox } from './styles'
import { Form, Input } from 'antd'
import { Icon } from '~/components/atoms'
import { Button } from '~/components/molecules'
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons'
import routeNames from '~/routes/routeNames'

const Login: React.FC<Props> = ({
    t,
    websiteInformation,
    isProcessing,
    userClickLogin,
    navigateTo,
}: Props) => {
    const onLogin = useCallback((value) => {
        // Modal.warning({
        //     title: 'Your account is invalid. Please try again',
        // })
        userClickLogin(value)
        // navigateTo(routeNames.ASSIGN_POS, false, value)
    }, [])

    return (
        <>
            <LogoBox>
                {websiteInformation.logo_url && (
                    <Logo src={websiteInformation.logo_url} alt={'logo'} />
                )}
            </LogoBox>

            <Form name="login" className="login-form" onFinish={onLogin}>
                <Form.Item
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Username!',
                        },
                    ]}
                >
                    <Input
                        prefix={
                            <Icon
                                name={'fas fa-user'}
                                color={'rgba(0, 0, 0, 0.25)'}
                            />
                        }
                        placeholder="Username"
                        size={'large'}
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input.Password
                        prefix={
                            <Icon
                                name={'fas fa-key'}
                                color={'rgba(0, 0, 0, 0.25)'}
                            />
                        }
                        placeholder="Password"
                        size={'large'}
                        iconRender={(visible) =>
                            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                        }
                    />
                </Form.Item>

                <Button
                    block
                    loading={isProcessing}
                    size={'large'}
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                >
                    LOGIN
                </Button>
            </Form>
        </>
    )
}

export default Login
