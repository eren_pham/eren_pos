import { getThemeColor } from '~/styles/themes/selectors'

export const getBoxShadow = getThemeColor('@box-shadow')
export const getBorderRadius = getThemeColor('@border-radius')
