import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import {
    actions as webposActions,
    selectors as webposSelectors,
} from '~/modules/Webpos'
import {
    actions as webposIDActions,
    selectors as webposIDSelectors,
    UserLoginForm,
} from '~/modules/WebposID'
import { push, replace } from 'connected-react-router'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    websiteInformation: webposSelectors.information(state),
    isProcessing: webposIDSelectors.isProcessing(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    userClickLogin: (formData: UserLoginForm): void => {
        dispatch(webposIDActions.userClickLogin(formData))
    },
    navigateTo: (path, isReplace?: boolean, params?: any) =>
        dispatch(isReplace ? replace(path, params) : push(path, params)),
})

export default connect(mapStateToProps, mapDispatchToProps)
