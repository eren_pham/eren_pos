import { withTranslation } from 'react-i18next'
import mapToProps from './mapToProps'
import requireAnonymousUser from '~/hoc/requireAnonymousUser'

export default [requireAnonymousUser(), mapToProps, withTranslation()]
