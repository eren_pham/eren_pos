import { RouteConfigComponentProps } from 'react-router-config'
import { WithTranslation } from 'react-i18next'
import { PosInterface } from '~/modules/Webpos'

export interface StateProps {
    readonly pos: PosInterface | null
}

export interface DispatchProps {
    readonly userClickLogout: () => void
    readonly assignPos: (pos: PosInterface) => void
    readonly navigateTo: (routeName: string, isReplace?: boolean) => void
}

export interface HandlerProps {}

export type Props = StateProps &
    DispatchProps &
    HandlerProps &
    RouteConfigComponentProps &
    WithTranslation
