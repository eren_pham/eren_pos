import React, { useCallback, useEffect, useState } from 'react'
import { Props } from '../types'
import { Text, TopContent } from './styles'
import { Form, Select } from 'antd'
import { Icon } from '~/components/atoms'
import { Button } from '~/components/molecules'
import * as Api from '~/data/Api'
import { PosInterface } from '~/modules/Webpos'
import routeNames from '~/routes/routeNames'

const { Option } = Select

const AssignPOS: React.FC<Props> = ({
    t,
    userClickLogout,
    assignPos,
    pos,
    navigateTo,
}: Props) => {
    const [fetching, setFetching] = useState<boolean>(true)
    const [listPos, setListPos] = useState<PosInterface[] | null>(null)

    useEffect(() => {
        Api.getListPos().subscribe((value) => {
            setListPos(value)
            setFetching(false)
        })
    }, [])

    useEffect(() => {
        pos !== null && navigateTo(routeNames.HOME)
    }, [pos])

    const onSelect = useCallback(
        (form) => {
            try {
                if (listPos !== null) {
                    const posSelected = listPos.filter(
                        (i) => i.pos_id === form.pos,
                    )[0]
                    assignPos(posSelected)
                }
            } catch (e) {}
        },
        [listPos],
    )

    return (
        <>
            <TopContent>
                <Icon name={'far fa-computer-speaker'} fontSize={64} />
                <Text>Select a POS</Text>
            </TopContent>

            <Form name="select-pos" className="login-form" onFinish={onSelect}>
                <Form.Item
                    name="pos"
                    rules={[
                        {
                            required: true,
                            message: 'Please select a POS!',
                        },
                    ]}
                >
                    <Select
                        loading={fetching}
                        size={'large'}
                        style={{ width: '100%' }}
                        placeholder={'Select a POS...'}
                    >
                        {listPos !== null &&
                            listPos.map((item, index) => (
                                <Option value={item.pos_id}>
                                    {item.pos_name}
                                </Option>
                            ))}
                    </Select>
                </Form.Item>

                <Form.Item>
                    <Button
                        block
                        size={'large'}
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                    >
                        Select POS
                    </Button>
                </Form.Item>
            </Form>
            <Button
                block
                size={'large'}
                htmlType="submit"
                className="login-form-button"
                onClick={userClickLogout}
            >
                Logout
            </Button>
        </>
    )
}

export default AssignPOS
