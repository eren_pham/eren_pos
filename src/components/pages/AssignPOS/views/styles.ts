import styled from '~/styles/styled'

export const TopContent = styled.div`
    text-align: center;
    width: 250px;
    height: 100px;
    margin: 25px auto 50px;
`
export const Text = styled.div`
    font-size: 18px;
    font-weight: 600;
    margin-top: 35px;
`
