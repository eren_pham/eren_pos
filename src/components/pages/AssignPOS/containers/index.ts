import { withTranslation } from 'react-i18next'
import mapToProps from './mapToProps'
import requireAuthUser from '~/hoc/requireAuthUser'

export default [requireAuthUser(), mapToProps, withTranslation()]
