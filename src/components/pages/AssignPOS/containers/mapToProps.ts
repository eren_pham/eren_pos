import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import Message from '~/lib/utils/Message'
import {
    actions as webposActions,
    selectors as webposSelectors,
    PosInterface,
} from '~/modules/Webpos'
import { actions as webposIDActions } from '~/modules/WebposID'
import { push, replace } from 'connected-react-router'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    pos: webposSelectors.pos(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    userClickLogout: (): void => {
        Message("You've been logged out")
        dispatch(webposIDActions.userClickLogout())
    },
    assignPos: (pos: PosInterface): void => {
        dispatch(webposActions.assignPos(pos))
    },
    navigateTo: (path, isReplace?: boolean) =>
        dispatch(isReplace ? replace(path) : push(path)),
})

export default connect(mapStateToProps, mapDispatchToProps)
