import { compose } from 'recompose'
import containers from './containers'
import { Props } from './types'
import AssignPOSPage from './views'

export default compose<Props, {}>(...containers)(AssignPOSPage)
