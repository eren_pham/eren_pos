import styled from 'styled-components'
import { createGlobalStyle, css } from '~/styles/styled'
import { getBackgroundColor } from './themes'

export const GlobalStyle = createGlobalStyle`
    html, body {
         background: ${getBackgroundColor} !important;
    }
`

export const rootStyle = css`
    width: 100vw;
    height: 100vh;
`

export const RootContainer = styled.div`
    ${rootStyle};
`
