import { getThemeColor } from '~/styles/themes/selectors'

export const getBackgroundColor = getThemeColor('@body-background')
