import * as React from 'react'
import { useEffect } from 'react'
import { Helmet, HelmetProps } from 'react-helmet'
import { renderRoutes } from 'react-router-config'
import { Props } from '../types'
import { GlobalStyle, RootContainer } from './styles'
import { LoadingBar } from '~/components/atoms'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { QUOTE } from '~/modules/Webpos/constants'
import * as Api from '~/data/Api'
import { newCart } from '~/modules/Webpos/actions'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

const meta: HelmetProps['meta'] = [
    {
        charSet: 'utf-8',
    },
]

const Root: React.FC<Props> = ({
    route,
    getWebsiteInformation,
    userRestoreLogin,
    newCart,
}) => {
    useEffect(() => {
        getWebsiteInformation()
    }, [])

    useEffect(() => {
        userRestoreLogin()
    }, [])

    useEffect(() => {
        !storage.get(QUOTE) && newCart()
    }, [])

    useEffect(() => {
        window.addEventListener('online', () => {
            console.log('online')
        })
        window.addEventListener('offline', () => {
            console.log('offline')
        })
        return () => {
            window.removeEventListener('online', () => {})
            window.removeEventListener('offline', () => {})
        }
    }, [])

    return (
        <>
            <Helmet
                defaultTitle={process.env.REACT_APP_APP_NAME}
                titleTemplate={'%s | ' + process.env.REACT_APP_APP_NAME}
                meta={meta}
            />

            <RootContainer>
                <GlobalStyle />
                <React.Suspense fallback={<LoadingBar />}>
                    {route && renderRoutes(route.routes)}
                </React.Suspense>
            </RootContainer>
        </>
    )
}

export default Root
