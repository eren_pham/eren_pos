import { RouteConfigComponentProps } from 'react-router-config'
import { StaffInterface } from '~/modules/WebposID'

export interface StateProps {
    readonly staff: StaffInterface | null
}

export interface DispatchProps {
    readonly getWebsiteInformation: () => void
    readonly navigateTo: (routeName: string, isReplace?: boolean) => void
    readonly userRestoreLogin: () => void
    readonly newCart: () => void
}

export type Props = StateProps & DispatchProps & RouteConfigComponentProps
