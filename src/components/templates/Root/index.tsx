import { compose } from 'recompose'
import containers from './containers'
import { Props } from './types'
import RootTemplate from './views'

export default compose<Props, {}>(...containers)(RootTemplate)
