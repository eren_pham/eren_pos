import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import {
    actions as webposIDActions,
    selectors as webposIDSelectors,
} from '~/modules/WebposID'
import {
    actions as webposActions,
    selectors as webposSelectors,
} from '~/modules/Webpos'
import { push, replace } from 'connected-react-router'
import { newCart } from '~/modules/Webpos/actions'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    staff: webposIDSelectors.staff(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    getWebsiteInformation: (): void => {
        dispatch(webposActions.getWebsiteInformation())
    },
    navigateTo: (path, isReplace?: boolean) =>
        dispatch(isReplace ? replace(path) : push(path)),
    userRestoreLogin: (): void => {
        dispatch(webposIDActions.userRestoreLogin())
    },
    newCart: (): void => {
        dispatch(webposActions.newCart())
    },
})

export default connect(mapStateToProps, mapDispatchToProps)
