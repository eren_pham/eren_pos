import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import { actions as webposIDActions } from '~/modules/WebposID'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({})

export default connect(mapStateToProps, mapDispatchToProps)
