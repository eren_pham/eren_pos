import { RouteConfigComponentProps } from 'react-router-config'
import { StateHandlerMap } from 'recompose'

export interface StateProps {}

export interface DispatchProps {}

export interface LocalStateProps {}

export interface StateUpdaterProps extends StateHandlerMap<LocalStateProps> {}

export interface ExternalProps {}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    LocalStateProps &
    StateUpdaterProps &
    RouteConfigComponentProps
