import * as React from 'react'
import { renderRoutes } from 'react-router-config'
import { Props } from '../types'
import { Sidebar } from '~/components/organisms'
import { LoadingBar } from '~/components/atoms'

const AppTemplate: React.FC<Props> = ({ route, isProcessing }: Props) => {
    const pageRef = React.useRef(null)

    return !isProcessing ? (
        <div style={{ display: 'flex', height: '100vh', overflow: 'hidden' }}>
            <React.Suspense fallback={<LoadingBar />}>
                {route && renderRoutes(route.routes, { pageRef })}
            </React.Suspense>
            <Sidebar />
        </div>
    ) : (
        <></>
    )
}

export default AppTemplate
