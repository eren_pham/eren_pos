import { withTranslation } from 'react-i18next'
import mapToProps from './mapToProps'

export default [withTranslation(), mapToProps]
