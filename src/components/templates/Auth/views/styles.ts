import styled from '~/styles/styled'
import { getBorderRadius, getBoxShadow } from './themes'

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    bottom: 80px;
    margin: 0;
    padding: 0;
    height: 100vh;
    width: 100vw;
`

export const Container = styled.div`
    display: block;
    padding: 30px;
    width: 470px;
    background: #fff;
    border-radius: ${getBorderRadius};
    box-shadow: ${getBoxShadow};
`
