import * as React from 'react'
import { renderRoutes } from 'react-router-config'
import { Props } from '../types'
import { Container, Wrapper } from './styles'

const AuthTemplate: React.FC<Props> = React.memo(({ route }: Props) => {
    return (
        <Wrapper>
            <Container>{route && renderRoutes(route.routes)}</Container>
        </Wrapper>
    )
})

export default AuthTemplate
