import { RouteConfigComponentProps } from 'react-router-config'
import { WithTranslation } from 'react-i18next'

export interface StateProps {}

export interface DispatchProps {}

export type Props = StateProps &
    DispatchProps &
    RouteConfigComponentProps &
    WithTranslation
