import { RouteConfigComponentProps } from 'react-router-config'
import { StaffInterface } from '~/modules/WebposID'
import { AddToCartParams, PosInterface } from '~/modules/Webpos'

export interface StateProps {
    readonly staff: StaffInterface | null
    readonly pos: PosInterface | null
    readonly quoteId: string | null
}

export interface DispatchProps {
    readonly addToCart: (item: AddToCartParams) => void
}

export interface ExternalProps {}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
