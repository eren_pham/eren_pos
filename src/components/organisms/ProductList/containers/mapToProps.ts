import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import {
    actions as webposActions,
    AddToCartParams,
    selectors as webposSelectors,
} from '~/modules/Webpos'
import { selectors as webposIDSelectors } from '~/modules/WebposID'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    staff: webposIDSelectors.staff(state),
    pos: webposSelectors.pos(state),
    quoteId: webposSelectors.quoteId(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    addToCart: (item: AddToCartParams): void => {
        dispatch(webposActions.addToCart(item))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)
