import { getThemeColor } from '~/styles/themes/selectors'

export const getBorderColor = getThemeColor('@border-color')
export const getBoxShadow = getThemeColor('@box-shadow')
export const getBorderRadius = getThemeColor('@border-radius')
