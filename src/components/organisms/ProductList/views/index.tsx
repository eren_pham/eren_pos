import React, {
    ChangeEvent,
    useCallback,
    useEffect,
    useRef,
    useState,
} from 'react'
import {
    Container,
    LogoBox,
    NavContainer,
    NavContent,
    ProductListBox,
    ProductListWrapper,
} from './styles'
import { Icon, Navbar } from '~/components/atoms'
import { Props } from '../types'
import { Col, Input, Modal, Radio, Row } from 'antd'
import * as Api from '~/data/Api'
import { ProductInterface, ProductResultInterface } from '~/modules/Webpos'
import { ProductItem, Spin } from '~/components/molecules'
import Scrollbar from 'smooth-scrollbar'

const ProductList = ({ staff, pos, quoteId, addToCart }: Props) => {
    const [currentPage, setCurrentPage] = useState<number>(1)
    const [fetchRequest, setFetchRequest] = useState<boolean>(false)
    const [modalData, setModalData] = useState<ProductInterface | null>(null)
    const [keyword, setKeyword] = useState()
    const [products, setProducts] = useState<ProductInterface[] | null>(null)
    const refSearching = useRef<any>()
    const [configurableQty, setConfigurableQty] = useState<number>(1)
    const [configurableSku, setConfigurableSku] = useState<string>()
    const configurableSize = useRef<string>()
    const configurableColor = useRef<string>()

    /*
     * find node for using smooth-scrollbar
     */
    useEffect(() => {
        const interval = setInterval(() => {
            const elementCatalog = document.querySelector(
                '.catalog-list',
            ) as HTMLElement

            if (elementCatalog) {
                clearInterval(interval)
                Scrollbar.init(elementCatalog)
            }
        }, 300)
    }, [products])

    /*
     * Detect scroll to bottom to apply lazy loading
     */
    useEffect(() => {
        const interval = setInterval(() => {
            const elementScroll = document.querySelector(
                '.scroll-content',
            ) as HTMLElement

            if (elementScroll) {
                clearInterval(interval)
                elementScroll.addEventListener('wheel', function (e) {
                    const parent = elementScroll.parentElement as any
                    if (parent) {
                        const offset =
                            parent.getBoundingClientRect().top -
                            elementScroll.getBoundingClientRect().top
                        if (
                            offset >
                                parent.scrollHeight -
                                    parent.clientHeight -
                                    800 &&
                            !fetchRequest
                        ) {
                            // in here, we are at the bottom
                            setFetchRequest(true)
                            elementScroll.removeEventListener('wheel', () => {})
                        }
                    }
                })
            }
        }, 300)
    }, [products])

    /*
     * in bottom, increase current page by 1
     */
    useEffect(() => {
        fetchRequest && setCurrentPage(currentPage + 1)
    }, [fetchRequest])

    /*
     * fetch data if current page or keyword change
     */
    useEffect(() => {
        fetchData()
    }, [currentPage, keyword])

    /*
     * call api get product
     */
    const fetchData = useCallback(() => {
        Api.productList({
            pageSize: 100,
            currentPage,
            keyword,
        }).subscribe((result) => {
            setFetchRequest(false)
            refSearching.current = null
            // const result = value.response as ProductResultInterface
            let previousProduct = products || []
            if (currentPage === 1) {
                previousProduct = []
            }
            const productResult = [...previousProduct, ...result.items]
            setProducts(productResult)
        })
    }, [products, currentPage, keyword])

    /*
     * On search box are typing
     */
    const onSearchBoxTyping = useCallback(
        (e: ChangeEvent<HTMLInputElement>) => {
            const { value } = e.target
            if (refSearching.current) {
                clearTimeout(refSearching.current)
            }
            refSearching.current = setTimeout(() => {
                setKeyword(value)
                setCurrentPage(1)
            }, 800)
        },
        [],
    )

    /*
     * On product item click
     */
    const onProductClick = useCallback((item: ProductInterface) => {
        if (item.type_id === 'simple') {
            addToCart({
                sku: item.sku,
                qty: 1,
            })
        } else if (item.type_id === 'configurable') {
            setModalData(item)
        }
    }, [])

    const onOkModal = () => {
        !!configurableSku &&
            !!configurableQty &&
            addToCart({
                sku: configurableSku,
                qty: configurableQty,
            })
        setConfigurableSku('')
        setConfigurableQty(1)
        setModalData(null)
    }

    const onCancelModal = () => {
        setModalData(null)
    }

    const onFindSku = () => {
        try {
            if (
                modalData &&
                configurableColor.current &&
                configurableSize.current
            ) {
                const item = modalData.childItems.filter(
                    (i) =>
                        i.color.label === configurableColor.current &&
                        i.size.label === configurableSize.current,
                )[0]
                setConfigurableSku(item.sku)
            }
        } catch (e) {}
    }

    const onSizeClick = (value: string) => {
        configurableSize.current = value
        if (configurableColor.current) {
            onFindSku()
        }
    }

    const onColorClick = (value: string) => {
        configurableColor.current = value
        if (configurableSize.current) {
            onFindSku()
        }
    }

    return (
        <ProductListWrapper>
            <Navbar>
                <NavContainer>
                    <LogoBox>
                        {staff && staff.staff_name} / {pos && pos.pos_name}
                    </LogoBox>
                    <NavContent>
                        <Input
                            size="large"
                            suffix={<Icon name={'fas fa-search'} />}
                            onChange={onSearchBoxTyping}
                            allowClear
                        />
                    </NavContent>
                </NavContainer>
            </Navbar>
            <Container>
                {products === null ||
                !!refSearching.current ||
                quoteId === null ? (
                    <Spin iconSize={56} center />
                ) : (
                    <ProductListBox className={'catalog-list'}>
                        <Row gutter={[16, 16]}>
                            {products
                                .filter((i) => !i.name.includes('-'))
                                .map((item, index) => (
                                    <Col
                                        key={index}
                                        md={24}
                                        lg={12}
                                        xl={8}
                                        xxl={6}
                                    >
                                        <ProductItem
                                            product={item}
                                            onClick={() => onProductClick(item)}
                                        />
                                    </Col>
                                ))}
                        </Row>
                    </ProductListBox>
                )}
            </Container>
            <Modal
                title={
                    modalData && (
                        <div style={{ textAlign: 'center' }}>
                            {modalData.name}
                        </div>
                    )
                }
                visible={!!modalData}
                onOk={onOkModal}
                onCancel={onCancelModal}
                okText={configurableSku ? '(' + configurableSku + ')' : 'OK'}
                okButtonProps={{ disabled: !configurableSku }}
            >
                <Radio.Group
                    size={'large'}
                    defaultValue={undefined}
                    onChange={(e) => onSizeClick(e.target.value)}
                >
                    {modalData &&
                        modalData.childItems
                            .map((i) => i.size.label)
                            .filter((x, i, a) => a.indexOf(x) === i)
                            .map((item, key) => (
                                <Radio.Button key={key} value={item}>
                                    {item}
                                </Radio.Button>
                            ))}
                </Radio.Group>

                <div style={{ marginTop: 16, marginBottom: 16 }}>
                    <Radio.Group
                        size={'large'}
                        defaultValue={undefined}
                        onChange={(e) => onColorClick(e.target.value)}
                    >
                        {modalData &&
                            modalData.childItems
                                .map((i) => i.color.label)
                                .filter((x, i, a) => a.indexOf(x) === i)
                                .map((item, key) => (
                                    <Radio.Button key={key} value={item}>
                                        {item}
                                    </Radio.Button>
                                ))}
                    </Radio.Group>
                </div>

                <Input
                    key={configurableQty}
                    size={'large'}
                    addonBefore={
                        <div
                            onClick={() =>
                                configurableQty > 1 &&
                                setConfigurableQty(configurableQty - 1)
                            }
                        >
                            <Icon
                                name={'fas fa-minus'}
                                style={{ width: 60, cursor: 'pointer' }}
                            />
                        </div>
                    }
                    addonAfter={
                        <div
                            onClick={() =>
                                setConfigurableQty(configurableQty + 1)
                            }
                        >
                            <Icon
                                name={'fas fa-plus'}
                                style={{ width: 60, cursor: 'pointer' }}
                            />
                        </div>
                    }
                    style={{ textAlign: 'center' }}
                    defaultValue={configurableQty}
                    onChange={(e) =>
                        setConfigurableQty(parseInt(e.target.value))
                    }
                />
            </Modal>
        </ProductListWrapper>
    )
}

export default ProductList
