import styled from '~/styles/styled'
import { getBorderColor, getBorderRadius, getBoxShadow } from './themes'
import { Spin } from 'antd'

export const ProductListWrapper = styled.div`
    height: 100vh;
    width: calc(100vw - 513px);
    background-color: #eef4fb;
    border-left: 0.5px ${getBorderColor} solid;
    border-rigth: 0.5px ${getBorderColor} solid;
`

export const Container = styled.div`
    position: relative;
    height: 100%;
`

export const ProductListBox = styled.div`
    overflow: scroll;
    height: calc(100vh - 64px);
    padding: 16px;
`

export const NavContainer = styled.div`
    width: 100%;
    max-height: 40px;
    display: flex;
`
export const LogoBox = styled.div`
    margin-left: 25px;
`

export const NavContent = styled.div`
    width: 65%;
    display: flex;
    position: absolute;
    right: 16px;
    top: 50%;
    transform: translateY(-50%);
`

export const SpinLoading = styled(Spin)`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`
