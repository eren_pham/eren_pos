import { compose } from 'recompose'
import containers from './containers'
import { ExternalProps, Props } from './types'
import ProductList from './views'

export default compose<Props, ExternalProps>(...containers)(ProductList)
