import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import {
    actions as webposActions,
    AssignCustomerParams,
    selectors as webposSelectors,
} from '~/modules/Webpos'
import { selectors as webposIDSelectors } from '~/modules/WebposID'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    accessToken: webposIDSelectors.accessToken(state),
    quoteId: webposSelectors.quoteId(state),
    isSignedIn: webposIDSelectors.isSignedIn(state),
    loading: webposSelectors.loading(state),
    cartLocal: webposSelectors.cart(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    setQuote: (quoteId: string): void => {
        dispatch(webposActions.setQuote(quoteId))
    },
    getCart: (): void => {
        dispatch(webposActions.getCart())
    },
    deleteCart: (itemId: number): void => {
        dispatch(webposActions.deleteCart(itemId))
    },
    newCart: (): void => {
        dispatch(webposActions.newCart())
    },
    startAssignCustomer: (assign: AssignCustomerParams): void => {
        dispatch(webposActions.startAssignCustomer(assign))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)
