import React, { useCallback, useEffect, useRef, useState } from 'react'
import {
    CartWrapper,
    Checkout,
    Container,
    LoadingBox,
    Title,
    DeleteButton,
    CustomerBox,
} from './styles'
import { Icon, Navbar } from '~/components/atoms'
import Button from '~/components/molecules/Button'
import { Props } from '../types'
import * as Api from '~/data/Api'
import { CartItem, Spin } from '~/components/molecules'
import { Select, Tooltip } from 'antd'
import { CustomerInfoInterface, CustomerInterface } from '~/modules/Webpos'

const Cart = ({
    accessToken,
    quoteId,
    setQuote,
    isSignedIn,
    loading,
    getCart,
    deleteCart,
    newCart,
    cartLocal,
    startAssignCustomer,
}: Props) => {
    const [totalPrice, setTotalPrice] = useState<number>(0)
    const [cartQty, setCartQty] = useState<number>(0)
    const [customerList, setCustomerList] = useState<CustomerInfoInterface[]>(
        [],
    )
    const refSearching = useRef<any>()
    const [customerIdSelected, setCustomerIdSelected] = useState<number | null>(
        null,
    )

    useEffect(() => {
        quoteId === null && createNewCart()
    }, [quoteId])

    useEffect(() => {
        quoteId !== null && getCart()
    }, [quoteId])

    useEffect(() => {
        let total = 0,
            qty = 0
        if (cartLocal !== null) {
            Object.keys(cartLocal).forEach((i) => {
                total += cartLocal[i].price * cartLocal[i].qty
                qty += cartLocal[i].qty
            })
            setTotalPrice(total)
            setCartQty(qty)
        }
    }, [cartLocal])

    useEffect(() => {
        if (customerIdSelected && quoteId && accessToken) {
            startAssignCustomer({
                customerId: customerIdSelected,
                quoteId,
            })
        }
    }, [customerIdSelected, quoteId, accessToken])

    const onDelete = useCallback((itemId: number) => {
        deleteCart(itemId)
    }, [])

    const createNewCart = useCallback(() => {
        isSignedIn && newCart()
    }, [isSignedIn])

    const onSearch = useCallback((keyword: string) => {
        if (refSearching.current) {
            clearTimeout(refSearching.current)
        }
        refSearching.current = setTimeout(() => {
            if (accessToken && !!keyword) {
                Api.searchCustomer({
                    keyword,
                    accessToken,
                }).subscribe((value) => {
                    refSearching.current = null
                    setCustomerList(value.items)
                })
            } else {
                setCustomerList([])
            }
        }, 800)
    }, [])

    return (
        <CartWrapper>
            <Navbar>
                <Title>Cart {cartQty > 0 ? '(' + cartQty + ')' : ''}</Title>
                {cartQty > 0 && (
                    <Tooltip placement="bottomRight" title={'Clear all'}>
                        <DeleteButton onClick={createNewCart}>
                            <Icon name={'fas fa-times'} />
                        </DeleteButton>
                    </Tooltip>
                )}
            </Navbar>
            <CustomerBox>
                <Select
                    showSearch
                    allowClear
                    size={'large'}
                    style={{ marginBottom: 16, width: '100%' }}
                    placeholder="Select a person"
                    optionFilterProp="children"
                    onChange={(value) => {
                        if (!value) {
                            createNewCart()
                        } else {
                            setCustomerIdSelected(parseInt(value + ''))
                        }
                    }}
                    onSearch={onSearch}
                    filterOption={(input, option) =>
                        option?.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                    }
                >
                    {customerList.map((item, index) => (
                        <Select.Option value={item.id}>
                            {item.firstname +
                                ' ' +
                                item.lastname +
                                ' (' +
                                item.email +
                                ')'}
                        </Select.Option>
                    ))}
                </Select>
            </CustomerBox>
            <LoadingBox style={{ height: loading ? 60 : 0 }}>
                <Spin size={'large'} />
            </LoadingBox>
            <Container>
                {cartLocal !== null &&
                    Object.keys(cartLocal)
                        .sort((a, b) =>
                            cartLocal[a].name.localeCompare(cartLocal[b].name),
                        )
                        .map((id, index) => (
                            <CartItem
                                item={cartLocal[id]}
                                onDelete={onDelete}
                            />
                        ))}
            </Container>
            <Checkout>
                <Button
                    loading={loading}
                    block
                    size={'large'}
                    type={'primary'}
                    disabled={customerIdSelected === null}
                    style={{ fontWeight: 'bold' }}
                >
                    ${totalPrice.toFixed(2)}
                </Button>
            </Checkout>
        </CartWrapper>
    )
}

export default Cart
