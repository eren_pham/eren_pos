import styled from '~/styles/styled'
import { getBorderColor } from './themes'
import { getDisabledColor } from '~/components/molecules/SidebarItem/views/themes'

export const CartWrapper = styled.div`
    height: 100vh;
    width: 425px;
    background-color: #fff;
    position: relative;
    border-left: 0.5px ${getBorderColor} solid;
    border-rigth: 0.5px ${getBorderColor} solid;
`

export const Container = styled.div`
    height: calc(100vh - 64px - 72px - 72px);
    position: relative;
    overflow: scroll;
`

export const Title = styled.div`
    width: 100%;
    height: 100%;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    font-size: 1.2rem;
`

export const Checkout = styled.div`
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 72px;
    padding: 16px;
    background-color: #f0f0f0;
`

export const CustomerBox = styled.div`
    //position: absolute;
    //top: 64px;
    width: 100%;
    height: 72px;
    padding: 16px;
    background-color: #f0f0f0;
`
export const LoadingBox = styled.div`
    transition: height 0.5s;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    overflow: hidden;
`

export const DeleteButton = styled.div`
    position: absolute;
    top: 50%;
    right: 16px;
    transform: translate(-50%, -50%);
    cursor: pointer;
    > i {
        font-size: 18px;
        color: ${getDisabledColor};
        transition: all 0.3s;
        :hover {
            transform: scale(1.6);
            color: rgba(0, 0, 0, 0.65) !important;
        }
    }
`
