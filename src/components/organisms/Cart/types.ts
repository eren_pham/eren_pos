import { RouteConfigComponentProps } from 'react-router-config'
import { CartLocalInterface } from '~/modules/Webpos'
import { AssignCustomerParams } from '~/modules/Webpos/types'

export interface StateProps {
    readonly accessToken?: string
    readonly quoteId: string | null
    readonly isSignedIn: boolean
    readonly loading: boolean
    readonly cartLocal: CartLocalInterface | null
}

export interface DispatchProps {
    readonly setQuote: (quote: string) => void
    readonly getCart: () => void
    readonly deleteCart: (itemId: number) => void
    readonly newCart: () => void
    readonly startAssignCustomer: (assign: AssignCustomerParams) => void
}

export interface ExternalProps {}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
