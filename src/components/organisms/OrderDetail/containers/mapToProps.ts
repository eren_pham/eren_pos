import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({})

export default connect(mapStateToProps, mapDispatchToProps)
