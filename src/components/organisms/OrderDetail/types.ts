import { RouteConfigComponentProps } from 'react-router-config'
import { InformationInterface } from '~/modules/Webpos'

export interface StateProps {}

export interface DispatchProps {}

export interface ExternalProps {}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
