import React from 'react'
import {
    Container,
    NavContainer,
    OrderDetailBox,
    OrderDetailWrapper,
} from './styles'
import { Navbar } from '~/components/atoms'
import { Props } from '../types'

const OrderDetail = (props: Props) => {
    return (
        <OrderDetailWrapper>
            <Navbar>
                <NavContainer>ID-1021209323</NavContainer>
            </Navbar>
            <Container>
                <OrderDetailBox>OrderDetail</OrderDetailBox>
            </Container>
        </OrderDetailWrapper>
    )
}

export default OrderDetail
