import styled from '~/styles/styled'
import { getBorderColor } from './themes'

export const OrderDetailWrapper = styled.div`
    height: 100vh;
    width: calc(100vw - 513px);
    background-color: #eef4fb;
    border-left: 0.5px ${getBorderColor} solid;
    border-rigth: 0.5px ${getBorderColor} solid;
`

export const Container = styled.div``

export const OrderDetailBox = styled.div``

export const NavContainer = styled.div`
    width: 100%;
    max-height: 40px;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    font-size: 1.2rem;
`
