import { compose } from 'recompose'
import containers from './containers'
import { ExternalProps, Props } from './types'
import Sidebar from './views'

export default compose<Props, ExternalProps>(...containers)(Sidebar)
