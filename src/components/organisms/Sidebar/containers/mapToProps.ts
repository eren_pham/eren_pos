import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import { push, replace } from 'connected-react-router'
import { actions as webposIDActions } from '~/modules/WebposID'
import Message from '~/lib/utils/Message'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    pathname: state.router.location.pathname,
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({
    navigateTo: (path, isReplace?: boolean) =>
        dispatch(isReplace ? replace(path) : push(path)),
    userClickLogout: (): void => {
        Message("You've been logged out")
        dispatch(webposIDActions.userClickLogout())
    },
})

export default connect(mapStateToProps, mapDispatchToProps)
