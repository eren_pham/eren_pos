import styled from '~/styles/styled'
import { getBorderColor } from './themes'

export const SidebarWrapper = styled.div`
    height: 100vh;
    width: 88px;
    background-color: #fff;
    border-left: 0.5px ${getBorderColor} solid;
    border-rigth: 0.5px ${getBorderColor} solid;
`

export const Container = styled.div``

export const SidebarBox = styled.div``
