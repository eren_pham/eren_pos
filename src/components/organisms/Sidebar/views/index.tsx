import React, { useCallback } from 'react'
import { Container, SidebarBox, SidebarWrapper } from './styles'
import { Navbar } from '~/components/atoms'
import { SidebarItem } from '~/components/molecules'
import { Props } from '../types'
import routeNames from '~/routes/routeNames'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { WEBSITE_INFORMATION } from '~/modules/Webpos/constants'
import Message from '~/lib/utils/Message'

/**
 * Storage client
 */
const storage = new LocalStorageClient()

const Sidebar = ({ navigateTo, pathname, userClickLogout }: Props) => {
    const clearCache = useCallback(() => {
        storage.remove(WEBSITE_INFORMATION)
        Message('Clearing cache')
        setTimeout(() => {
            window.location.reload()
        }, 500)
    }, [])

    return (
        <SidebarWrapper>
            <Navbar style={{ textAlign: 'center' }} />
            <Container>
                <SidebarBox>
                    <SidebarItem
                        active={pathname === routeNames.HOME}
                        icon={'fas fa-shopping-cart'}
                        text={'Checkout'}
                        action={() => navigateTo(routeNames.HOME)}
                    />
                    <SidebarItem
                        active={pathname === routeNames.ORDER}
                        icon={'fas fa-receipt'}
                        text={'Order'}
                        action={() => navigateTo(routeNames.ORDER)}
                    />
                    <SidebarItem
                        icon={'fas fa-retweet'}
                        text={'ClearCache'}
                        action={clearCache}
                    />
                    <SidebarItem
                        icon={'fas fa-sign-out-alt'}
                        text={'Logout'}
                        action={userClickLogout}
                    />
                </SidebarBox>
            </Container>
        </SidebarWrapper>
    )
}

export default Sidebar
