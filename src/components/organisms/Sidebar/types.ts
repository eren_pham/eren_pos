import { RouteConfigComponentProps } from 'react-router-config'

export interface StateProps {
    readonly pathname: string
}

export interface DispatchProps {
    readonly navigateTo: (routeName: string, isReplace?: boolean) => void
    readonly userClickLogout: () => void
}

export interface ExternalProps {}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
