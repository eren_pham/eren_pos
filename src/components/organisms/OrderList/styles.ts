import styled from '~/styles/styled'
import { getBorderColor } from './themes'

export const OrderListWrapper = styled.div`
    height: 100vh;
    width: 425px;
    background-color: #fff;
    border-left: 0.5px ${getBorderColor} solid;
    border-rigth: 0.5px ${getBorderColor} solid;
`

export const Container = styled.div``

export const OrderListBox = styled.div``

export const Title = styled.div`
    width: 100%;
    height: 100%;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    font-size: 1.2rem;
`
