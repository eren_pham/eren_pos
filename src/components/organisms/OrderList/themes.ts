import { getThemeColor } from '~/styles/themes/selectors'

export const getBorderColor = getThemeColor('@border-color')
