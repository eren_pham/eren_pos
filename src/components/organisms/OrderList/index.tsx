import React from 'react'
import { OrderListBox, OrderListWrapper, Container, Title } from './styles'
import { Navbar } from '~/components/atoms'

interface OrderListProps {}

const OrderList = (props: OrderListProps) => {
    return (
        <OrderListWrapper>
            <Navbar>
                <Title>Orders</Title>
            </Navbar>
            <Container>
                <OrderListBox>OrderList</OrderListBox>
            </Container>
        </OrderListWrapper>
    )
}

export default OrderList
