import React from 'react'
import { Props } from '../types'
import { LoadingOutlined } from '@ant-design/icons'
import { Spin } from 'antd'

const SpinLoading = (props: Props) => {
    const primaryColor = props.websiteInformation.primary_color
    return (
        <Spin
            style={
                props.center
                    ? {
                          position: 'absolute',
                          transform: 'translate(-50%,-50%)',
                          top: '50%',
                          left: '50%',
                      }
                    : {}
            }
            {...props}
            indicator={
                <LoadingOutlined
                    spin
                    style={{
                        fontSize: props.iconSize,
                        color: primaryColor ? primaryColor : props.color,
                    }}
                />
            }
        />
    )
}

export default SpinLoading
