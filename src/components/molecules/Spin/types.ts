import { RouteConfigComponentProps } from 'react-router-config'
import { InformationInterface } from '~/modules/Webpos'
import { SpinProps } from 'antd/lib/spin'

export interface StateProps {
    readonly websiteInformation: InformationInterface
}

export interface DispatchProps {}

export interface ExternalProps extends SpinProps {
    color?: string
    iconSize?: number
    center?: boolean
}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
