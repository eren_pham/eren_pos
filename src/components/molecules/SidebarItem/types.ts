import { RouteConfigComponentProps } from 'react-router-config'
import { InformationInterface } from '~/modules/Webpos'

export interface StateProps {
    readonly websiteInformation: InformationInterface
}

export interface DispatchProps {}

export interface ExternalProps {
    active?: boolean
    text: string
    icon: string
    action: () => void
}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
