import styled from 'styled-components'
import { css } from '~/styles/styled'
import { getPrimaryBg, getDisabledColor } from './themes'

export const Container: any = styled.div`
    width: 88px;
    height: 88px;
    cursor: pointer;
    letter-spacing: 0.025em;
    color: ${getDisabledColor};
    text-align: center;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    > .active {
        color: ${(props: any) =>
            props.primaryColor ? props.primaryColor : getPrimaryBg};
    }
    :hover {
        transition: all 0.3s;
        color: #fff !important;
        > .active {
            color: #fff !important;
        }
        background-color: ${(props: any) =>
            props.hoverColor ? props.hoverColor : 'transparent'};
    }
`

export const TextItem = styled.div`
    text-transform: uppercase;
    font-size: 11px;
    margin-top: 8px;
`
