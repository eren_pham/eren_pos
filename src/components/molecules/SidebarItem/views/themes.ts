import { getThemeColor } from '~/styles/themes/selectors'

export const getPrimaryBg = getThemeColor('@btn-primary-bg')
export const getDisabledColor = getThemeColor('@disabled-color')
