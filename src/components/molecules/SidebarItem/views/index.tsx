import React from 'react'
import { Container, TextItem } from './styles'
import { Props } from '../types'
import { Icon } from '~/components/atoms'
import { whiten } from '~/styles/colors'

const SidebarItem = React.memo((props: Props) => {
    const primaryColor = props.websiteInformation.primary_color
    return (
        <Container
            onClick={props.action}
            primaryColor={primaryColor && props.active ? primaryColor : null}
            hoverColor={primaryColor ? primaryColor : null}
        >
            <Icon
                name={props.icon}
                fontSize={24}
                className={props.active ? 'active' : ''}
                // color={primaryColor || undefined}
            />
            <TextItem
                className={props.active ? 'active' : ''}
                // style={primaryColor ? { color: primaryColor } : {}}
            >
                {props.text}
            </TextItem>
        </Container>
    )
})

export default SidebarItem
