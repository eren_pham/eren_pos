import { RouteConfigComponentProps } from 'react-router-config'
import { ProductInterface } from '~/modules/Webpos'

export interface StateProps {}

export interface DispatchProps {}

export interface ExternalProps {
    readonly product: ProductInterface
    readonly onClick: () => void
}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
