import React from 'react'
import { Center, Container, Content, Image, Row } from './styles'
import { Props } from '../types'

const ProductItem = React.memo(({ product, onClick }: Props) => {
    return (
        <Container onClick={onClick}>
            <Center>
                <Image src={product.image} alt={product.sku} />
            </Center>
            <Content>
                <Row>{product.name}</Row>
                <Row>
                    <span style={{ fontSize: 12 }}>[{product.sku}]</span>
                </Row>
                <Row>${product.price.toFixed(2)}</Row>
                <Row>{product.type_id}</Row>
            </Content>
        </Container>
    )
})

export default ProductItem
