import styled from 'styled-components'
import { getBorderRadius, getBoxShadow } from './themes'

export const Container = styled.div`
    padding: 16px;
    font-size: 14px;
    background: #fff;
    transition: all 0.3s;
    height: 100%;
    :hover {
        cursor: pointer;
        border-radius: ${getBorderRadius};
        box-shadow: ${getBoxShadow};
        transform: scale(1.1);
    }
`

export const Center = styled.div`
    text-align: center;
`

export const Image = styled.img`
    width: 60%;
`

export const Content = styled.div`
    width: 100%;
    height: 100%;
`

export const Row = styled.div``
