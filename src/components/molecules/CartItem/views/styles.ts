import styled from 'styled-components'
import { getBorderColor } from './themes'
import { getDisabledColor } from '~/components/molecules/SidebarItem/views/themes'

export const Container = styled.div`
    font-size: 14px;
    background: #fff;
    transition: all 0.3s;
    height: 88px;
    border-bottom: 1px #f2f2f2 solid;
    display: flex;
    position: relative;
`

export const Image = styled.img`
    width: 60px;
    height: 60px;
`

export const Padding = styled.div`
    padding: 16px;
`

export const ProductInfo = styled.div`
    margin-left: 20px;
    width: 250px;
    font-size: 14px;
    margin-top: 8px;
    > :nth-child(2) {
        font-size: 12px;
    }
`

export const DeleteButton = styled.div`
    position: absolute;
    top: 50%;
    right: 16px;
    transform: translate(-50%, -50%);
    cursor: pointer;
    > i {
        font-size: 18px;
        color: ${getDisabledColor};
        transition: all 0.3s;
        :hover {
            transform: scale(1.6);
            color: rgba(0, 0, 0, 0.65) !important;
        }
    }
`
