import React from 'react'
import { Container, Image, ProductInfo, DeleteButton, Padding } from './styles'
import { Props } from '../types'
import { Badge } from 'antd'
import { Icon } from '~/components/atoms'

const CartItem = ({ item, onDelete, loading }: Props) => {
    return (
        <Container>
            <Padding>
                {item.qty > 1 ? (
                    <Badge count={item.qty} offset={[0, 0]}>
                        <Image
                            src={item.extension_attributes.image_url}
                            alt={''}
                        />
                    </Badge>
                ) : (
                    <Image src={item.extension_attributes.image_url} alt={''} />
                )}
            </Padding>
            <ProductInfo>
                <div>{item.name}</div>
                <div>[{item.sku}]</div>
                <div style={{ fontWeight: 'bold' }}>
                    ${(item.qty * item.price).toFixed(2)}
                </div>
            </ProductInfo>
            {!loading && (
                <DeleteButton onClick={() => onDelete(item.item_id)}>
                    <Icon name={'fas fa-trash'} />
                </DeleteButton>
            )}
        </Container>
    )
}

export default CartItem
