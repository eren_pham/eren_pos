import { connect } from 'react-redux'
import { MapDispatchToProps, MapStateToProps } from '~/types/redux'
import { DispatchProps, StateProps } from '../types'
import { selectors as webposSelectors } from '~/modules/Webpos'

const mapStateToProps: MapStateToProps<StateProps, {}> = (state) => ({
    loading: webposSelectors.loading(state),
})

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = (
    dispatch,
) => ({})

export default connect(mapStateToProps, mapDispatchToProps)
