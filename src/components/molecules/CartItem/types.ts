import { RouteConfigComponentProps } from 'react-router-config'
import { CartItemInterface } from '~/modules/Webpos'

export interface StateProps {
    readonly loading: boolean
}

export interface DispatchProps {}

export interface ExternalProps {
    readonly item: CartItemInterface
    readonly onDelete: (item_id: number) => void
}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
