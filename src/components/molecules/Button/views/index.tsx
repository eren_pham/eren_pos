import React from 'react'
import { Container } from './styles'
import { Props } from '../types'

const Button = React.memo((props: Props) => {
    const primaryColor = props.websiteInformation.primary_color
    let style: React.CSSProperties = {
        color: primaryColor || props.color,
        ...props.style,
    }

    if (props.type) {
        if (props.disabled) {
            style = {
                borderColor: primaryColor || props.color,
                color: 'rgba(0, 0, 0, 0.65)',
                ...props.style,
            }
        } else if (props.type !== 'link') {
            style = {
                backgroundColor: primaryColor || props.color,
                borderColor: primaryColor || props.color,
                ...props.style,
            }
        }
    }

    if (!!props.color || !!primaryColor) {
        return (
            <Container {...props} style={style}>
                {props.children}
            </Container>
        )
    }
    return <Container {...props}>{props.children}</Container>
})

export default Button
