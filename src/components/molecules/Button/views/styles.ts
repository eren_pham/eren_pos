import styled from 'styled-components'
import { Button } from 'antd'
import { css } from '~/styles/styled'

export const buttonStyle = css``

export const Container = styled(Button)`
    ${buttonStyle};
`
