import { RouteConfigComponentProps } from 'react-router-config'
import { ButtonProps } from 'antd/lib/button/button'
import { InformationInterface } from '~/modules/Webpos'

export interface StateProps {
    readonly websiteInformation: InformationInterface
}

export interface DispatchProps {}

export interface ExternalProps extends ButtonProps {
    color?: string
}

export type Props = ExternalProps &
    StateProps &
    DispatchProps &
    RouteConfigComponentProps
