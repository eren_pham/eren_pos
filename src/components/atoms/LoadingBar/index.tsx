import React, { useEffect } from 'react'
import NProgress from 'nprogress'

interface LoadingBarProps {}

const LoadingBar = (props: LoadingBarProps) => {
    useEffect(() => {
        NProgress.start()
        return () => {
            NProgress.done()
        }
    }, [])
    return <></>
}

export default LoadingBar
