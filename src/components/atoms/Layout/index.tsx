import React from 'react'
import { Container } from './styles'
import { BasicProps } from 'antd/lib/layout/layout'

const Layout = React.memo((props: BasicProps) => {
    return <Container {...props} />
})

export default Layout
