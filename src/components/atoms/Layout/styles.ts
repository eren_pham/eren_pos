import styled from 'styled-components'
import { css } from '~/styles/styled'
import { Layout } from 'antd'

export const layoutStyle = css`
    background: transparent !important;
`

export const Container = styled(Layout)`
    ${layoutStyle};
`
