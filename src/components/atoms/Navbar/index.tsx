import React from 'react'
import { Container } from './styles'

const Navbar = React.memo((props: React.HTMLAttributes<HTMLDivElement>) => {
    return <Container {...props} />
})

export default Navbar
