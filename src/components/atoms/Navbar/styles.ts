import styled from 'styled-components'
import { css } from '~/styles/styled'
import { getBorderColor } from './themes'

export const navbarStyle = css`
    width: 100%;
    height: 64px;
    background-color: #fff;
    border-bottom: 1px ${getBorderColor} solid;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: relative;
    margin: 0;
    padding: 0;
    z-index: 100;
`

export const Container = styled.div`
    ${navbarStyle};
`
