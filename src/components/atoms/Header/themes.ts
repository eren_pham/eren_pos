import { getThemeColor } from '~/styles/themes/selectors'

export const getHeaderBackgroundColor = getThemeColor(
    '@layout-header-background',
)
export const getTextColor = getThemeColor('@text-color')
