import styled from 'styled-components'
import { css } from '~/styles/styled'
import { getHeaderBackgroundColor, getTextColor } from './themes'
import { Layout } from 'antd'

export const headerStyle = css`
    padding: 0 !important;
    background-color: ${getHeaderBackgroundColor};
    color: ${getTextColor};
    box-shadow: none;
`

export const Container = styled(Layout.Header)`
    ${headerStyle};
`
