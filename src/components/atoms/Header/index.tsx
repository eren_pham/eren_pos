import React from 'react'
import { BasicProps } from 'antd/lib/layout/layout'
import { Container } from './styles'

const Header = React.memo((props: BasicProps) => {
    return <Container {...props}>{props.children}</Container>
})

export default Header
