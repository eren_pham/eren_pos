import styled from 'styled-components'
import { css } from '~/styles/styled'

export const boxStyle = css``

export const Container = styled.i`
    ${boxStyle};
    ::before {
        color: ${(props) => props.color};
    }
`
