import React, { CSSProperties } from 'react'
import { Container } from './styles'

const Icon = React.memo(
    (props: {
        name: string
        fontSize?: number
        left?: number
        color?: string
        className?: string
        style?: CSSProperties
    }) => {
        return (
            <Container
                {...props}
                className={
                    props.name + (props.className ? ' ' + props.className : '')
                }
                color={props.color}
                style={{
                    fontSize: props.fontSize,
                    ...props.style,
                }}
            />
        )
    },
)

export default Icon
