import { connectRouter } from 'connected-react-router'
import { Reducer, combineReducers } from 'redux'
import history from './history'

// Modules
import { reducer as appReducer } from './modules/App'
import { reducer as localeReducer } from './modules/Locale'
import { reducer as webposReducer } from './modules/Webpos'
import { reducer as webposIDReducer } from './modules/WebposID'

import { RootAction, RootState } from './types/redux'

// Services
const reducer: Reducer<RootState, RootAction> = combineReducers({
    app: appReducer,
    locale: localeReducer,
    webpos: webposReducer,
    webposID: webposIDReducer,
    router: connectRouter(history),
})

export default reducer
