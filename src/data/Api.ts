import { ajax } from 'rxjs/ajax'
import {
    AddToCartParams,
    AssignCustomerParams,
    CartInfo,
    CustomerResultInterface,
    InformationInterface,
    PosInterface,
    ProductResultInterface,
} from '~/modules/Webpos'
import { UserLoginForm } from '~/modules/WebposID'
import {
    addFilterGroup,
    addFilterItem,
    addFilters,
    searchCriteriaBuilder,
} from '~/lib/utils/SearchCriteriaBuilder'
import { catchError } from 'rxjs/operators'
import LocalStorageClient from '~/lib/Storage/LocalStorageClient'
import { CART_ID, QUOTE } from '~/modules/Webpos/constants'
import { STAFF } from '~/modules/WebposID/constants'

/**
 * Storage client
 */
const storage = new LocalStorageClient()
const headers = () => ({
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Bearer ' + (storage.get(STAFF)?.access_token || ''),
})
const getQuoteIdAndCartId = (requireNumber = false) => {
    const quoteId = storage.get(QUOTE)
    const cartId = storage.get(CART_ID)
    if (quoteId && cartId && parseInt(quoteId) == quoteId && requireNumber) {
        return requireNumber ? quoteId : cartId
    }
    return quoteId || cartId
}

export const BASE_URL = 'http://127.0.0.1:7085/rest/default'

interface ProductSearchParams {
    pageSize?: number
    currentPage?: number
    keyword?: string
}

interface CustomerSearchParams {
    pageSize?: number
    currentPage?: number
    keyword: string
    accessToken: string
}

const Api = () => {
    const quoteId = storage.get(QUOTE)
    let isGuest = true
    if (parseInt(quoteId) == quoteId) {
        isGuest = false
    }

    console.log('isGuest', isGuest)

    return {
        website_information: '/V1/webpos/website/information',
        staff_login: '/V1/webposid/staff/login',
        pos: '/V1/webpos/list',
        product: '/V1/webpos/products',
        customers_search: '/V1/customers/search',
        create_guest_cart: isGuest ? '/V1/guest-carts' : '/V1/carts',
        get_cart_info: isGuest
            ? '/V1/guest-carts/:quoteId'
            : '/V1/carts/:quoteId',
        add_to_cart: isGuest
            ? '/V1/guest-carts/:quoteId/items'
            : '/V1/carts/mine/items',
        delete_item: isGuest
            ? '/V1/guest-carts/:quoteId/items/:itemId'
            : '/V1/carts/:quoteId/items/:itemId',
        assign_customer: isGuest
            ? '/V1/guest-carts/:quoteId'
            : '/V1/carts/:quoteId',
    }
}

export const getWebsiteInformation = () =>
    ajax.getJSON<InformationInterface>(
        `${BASE_URL}` + Api().website_information,
        headers(),
    )

export const userLogin = (formData: UserLoginForm) =>
    ajax.post(`${BASE_URL}` + Api().staff_login, { staff: formData }, headers())

export const getListPos = () =>
    ajax.getJSON<PosInterface[]>(`${BASE_URL}` + Api().pos, headers())

export const productList = (query: ProductSearchParams) =>
    ajax.getJSON<ProductResultInterface>(
        `${BASE_URL}` +
            Api().product +
            '?' +
            searchCriteriaBuilder(
                query.pageSize || 16,
                query.currentPage || 1,
                addFilterGroup(
                    // addFilters(addFilterItem('hidden_on_pos', '1')),
                    addFilters(
                        addFilterItem('type_id', 'simple'),
                        addFilterItem('type_id', 'configurable'),
                    ),
                    addFilters(
                        addFilterItem(
                            'name',
                            '%' + (query.keyword || '') + '%',
                            'like',
                        ),
                        addFilterItem(
                            'sku',
                            '%' + (query.keyword || '') + '%',
                            'like',
                        ),
                    ),
                ),
            ),
        headers(),
    )

export const createGuestCart = () =>
    ajax.post(`${BASE_URL}` + Api().create_guest_cart, {}, headers())

export const getCartInfo = (quoteId: string) =>
    ajax
        .getJSON<CartInfo>(
            `${BASE_URL}` + Api().get_cart_info.replace(':quoteId', quoteId),
            headers(),
        )
        .pipe(
            catchError((err: any, caught) => {
                // big bug :(
                createGuestCart()
                return [{} as CartInfo]
            }),
        )

export const addToCart = (item: AddToCartParams & { quoteId: string }) =>
    ajax.post(
        `${BASE_URL}` + Api().add_to_cart.replace(':quoteId', item.quoteId),
        {
            cart_item: {
                sku: item.sku,
                qty: item.qty,
                quote_id: getQuoteIdAndCartId(true),
            },
        },
        headers(),
    )

export const deleteItem = (quoteId: string, item_id: number) =>
    ajax.delete(
        `${BASE_URL}` +
            Api()
                .delete_item.replace(':quoteId', quoteId)
                .replace(':itemId', item_id.toString()),
        headers(),
    )

export const searchCustomer = (query: CustomerSearchParams) =>
    ajax.getJSON<CustomerResultInterface>(
        `${BASE_URL}` +
            Api().customers_search +
            '?' +
            searchCriteriaBuilder(
                query.pageSize || 16,
                query.currentPage || 1,
                addFilterGroup(
                    addFilters(
                        addFilterItem(
                            'name',
                            '%' + (query.keyword || '') + '%',
                            'like',
                        ),
                        addFilterItem(
                            'email',
                            '%' + (query.keyword || '') + '%',
                            'like',
                        ),
                    ),
                ),
            ),
        headers(),
    )

export const assignCustomer = (assign: AssignCustomerParams) =>
    ajax.put(
        `${BASE_URL}` +
            Api().assign_customer.replace(':quoteId', assign.quoteId),
        {
            customer_id: assign.customerId,
            store_id: 1,
        },
        headers(),
    )
